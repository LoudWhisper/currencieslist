//
//  RatesListViewControllerTests.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 09.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import UIKit

@testable import CurrencyList

class RatesListViewControllerTests: ViewControllerTestCase {
    override func setUp() {
        super.setUp()
        
//        self.recordMode = true
    }
    
    override func tearDown() {
        super.tearDown()
    } 
}

// MARK: Tests

extension RatesListViewControllerTests {
    func testRatesListViewController() {
        let container = try! RatesContainerModel(currency: .USD, dataVerifier: DataVerifier())
        var rates: [RateModel] = []
        for currency in container.currencies {
            guard let rate = container.rateForCurrency[currency.rawValue] else {
                continue
            }
            rates.append(rate)
        }
        
        let servicesAssembly = ServicesAssembly()
        let screensAssembly = ScreensAssembly(withServicesAssembly: servicesAssembly)
        let viewController = screensAssembly.viewController(withType: .rates(screenContainer: .list(router: self.ratesRouter()))) as! RatesListViewController
        viewController.view.layoutIfNeeded()
        viewController.applyState(.suspense(currencyWithAmount: container.currencyWithAmount, rates: rates))
        
        self.snapshot(viewController: viewController, identifier: "RatesListViewController")
    }
}
