//
//  ViewControllerTestCase.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 09.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import FBSnapshotTestCase

@testable import CurrencyList

class ViewControllerTestCase: FBSnapshotTestCase {
    private let window = UIWindow()
    private var configuredRouterLaunch: LaunchRouter?
    private var configuredRatesRouter: RatesRouter?
    
    override func setUp() {
        super.setUp()
        
        self.isDeviceAgnostic = true
//        self.recordMode = true
    }
    
    override func tearDown() {
        super.tearDown()
    }
}

// MARK: Support Methods

internal extension ViewControllerTestCase {
    func launchRouter(withWindow window: UIWindow) -> LaunchRouter {
        let router = LaunchRouter()
        router.showInitialScreen(withWindow: window)
        return router
    }
    
    func ratesRouter() -> RatesRouter {
        if self.configuredRatesRouter != nil {
            return self.configuredRatesRouter!
        }
        
        if self.configuredRouterLaunch == nil {
            self.configuredRouterLaunch = self.launchRouter(withWindow: self.window)
        }
        
        let router = RatesRouter(withParentRouter: self.configuredRouterLaunch!, servicesAssembly: self.configuredRouterLaunch!.servicesAssembly, screensAssembly: self.configuredRouterLaunch!.screensAssembly, navigationController: self.configuredRouterLaunch!.navigationController)
        router.showInitialScreen(withWindow: window)
        self.configuredRatesRouter = router
        return router
    }
    
    func snapshot(viewController: UIViewController, identifier: String) {
        self.window.rootViewController = viewController
        
        FBSnapshotVerifyView(self.window, identifier: identifier)
        FBSnapshotVerifyLayer(self.window.layer, identifier: identifier)
    }
}
