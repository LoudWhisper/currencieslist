//
//  AddressesProviderTests.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 09.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import XCTest

@testable import CurrencyList

// MARK: Class Definition

class AddressesProviderTests: XCTestCase {
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
}

// MARK: Tests

extension AddressesProviderTests {
    func testRatesPathsCorrect() {
        let addressesProvider = self.addressesProvider()

        let pathType = AddressesProvider.Paths.rates(pathContainer: .list)
        self.check(pathType: pathType, withAddressesProvider: addressesProvider)
        
        switch pathType {
        case .rates(let pathContainer):
            switch pathContainer {
            case .list:
                // tested
                break
            }
        }
    }
    
    func testPaths() {
        let pathType = AddressesProvider.Paths.rates(pathContainer: .list)
        switch pathType {
        case .rates(_):
            // tested
            break
        }
    }
}

// MARK: Support Methods

private extension AddressesProviderTests {
    func addressesProvider() -> AddressesProvider {
        let addressesProvider = AddressesProvider(targetValuesProvider: TargetValuesProvider(dataVerifier: DataVerifier()))
        return addressesProvider
    }
    
    func check(pathType: AddressesProvider.Paths, withAddressesProvider addressesProvider: AddressesProvider) {
        let path = pathType.path
        self.check(path: path)
        
        let address = addressesProvider.address(forPathType: pathType)
        self.check(address: address)
    }
    
    func check(address: String) {
        let url = URL(string: address)
        XCTAssertNotNil(address)
        XCTAssertNotNil(url)
        XCTAssertNotNil(url!.scheme)
        XCTAssertNotNil(url!.host)
        
        if !address.hasPrefix("https://") {
            XCTFail("\(address) must start with 'https://'")
        }
    }
    
    func check(path: String) {
        XCTAssertNotNil(path)
        XCTAssertFalse(path.isEmpty)
        XCTAssertFalse(path.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)
        
        if path.hasPrefix("/") {
            XCTFail("\(path) can't start with '/'")
        }
        
        if path.contains("//") {
            XCTFail("\(path) contain '//', probably empty query parameter")
        }
    }
}
