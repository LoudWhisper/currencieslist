//
//  ApplicationColorsTests.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 09.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import XCTest

@testable import CurrencyList

// MARK: Class Definition

class ApplicationColorsTests: XCTestCase {
    typealias ColorComponents = (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat)
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
}

// MARK: Tests

extension ApplicationColorsTests {
    func testColorsNotNil() {
        var applicationColor = ApplicationColor.cell
        XCTAssertNotNil(applicationColor.color)
        
        applicationColor = ApplicationColor.underline
        XCTAssertNotNil(applicationColor.color)
        
        applicationColor = ApplicationColor.view
        XCTAssertNotNil(applicationColor.color)
        
        applicationColor = ApplicationColor.text
        XCTAssertNotNil(applicationColor.color)
        
        switch applicationColor {
        case .cell, .underline, .view, .text:
            // tested
            break
        }
    }
}
