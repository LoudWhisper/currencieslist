//
//  ColorsTests.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 09.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import XCTest

@testable import CurrencyList

// MARK: Class Definition

class ColorsTests: XCTestCase {
    typealias ColorComponents = (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat)
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
}

// MARK: Tests

extension ColorsTests {
    func testColorsNotNil() {
        let hexColor = UIColor(hexString: "#0174B3")
        XCTAssertNotNil(hexColor)
        
        let rgbColor = UIColor(red: 1, green: 116, blue: 179)
        XCTAssertNotNil(rgbColor)
        
        let rgbColorWithAlpha = UIColor(red: 1, green: 116, blue: 179, alpha: 0.5)
        XCTAssertNotNil(rgbColorWithAlpha)
    }
    
    func testColorsEqual() {
        let referenceColor = UIColor(red: 1.0 / 255.0, green: 116.0 / 255.0, blue: 179.0 / 255.0, alpha: 0.5)
        let hexColor = UIColor(hexString: "#0174B3")
        let rgbColor = UIColor(red: 1, green: 116, blue: 179)
        let rgbAlphaColor = UIColor(red: 1, green: 116, blue: 179, alpha: 0.5)
        
        let referenceColorComponents = self.colorComponents(referenceColor)
        let hexColorComponents = self.colorComponents(hexColor)
        let rgbColorComponents = self.colorComponents(rgbColor)
        let rgbAlphaColorComponents = self.colorComponents(rgbAlphaColor)
        
        let colorsHasSameComponents = (self.components(referenceColorComponents, equalTo: hexColorComponents, withAlpha: false) || self.components(referenceColorComponents, equalTo: rgbColorComponents, withAlpha: false) || self.components(referenceColorComponents, equalTo: rgbAlphaColorComponents, withAlpha: true))
        XCTAssertTrue(colorsHasSameComponents)
    }
}

// MARK: Support Methods

private extension ColorsTests {
    func colorComponents(_ color: UIColor) -> ColorComponents {
        var red: CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue: CGFloat = 0.0
        var alpha: CGFloat = 0.0
        color.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        return (red: red, green: green, blue: blue, alpha: alpha)
    }
    
    func components(_ firstComponents: ColorComponents, equalTo secondComponents: ColorComponents, withAlpha: Bool) -> Bool {
        return (firstComponents.red == secondComponents.red && firstComponents.green == secondComponents.green && firstComponents.blue == secondComponents.blue) && (!withAlpha || (firstComponents.alpha == secondComponents.alpha))
    }
}
