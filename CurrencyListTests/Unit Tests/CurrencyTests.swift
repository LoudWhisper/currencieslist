//
//  CurrencyTests.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 09.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import XCTest

@testable import CurrencyList

// MARK: Class Definition

class CurrencyTests: XCTestCase {
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
}

// MARK: Tests

extension CurrencyTests {
    func testValues() {
        let values = Currency.values()
        XCTAssertFalse(values.contains(.undefined))
        XCTAssertTrue(Currency.count() == values.count)
    }
    
    func testCountries() {
        let values = Currency.values()
        for value in values {
            XCTAssertNotNil(value.country)
        }
    }
    
    func testCountryImages() {
        let values = Currency.values()
        for value in values {
            XCTAssertNotNil(value.countryImage)
        }
    }
}
