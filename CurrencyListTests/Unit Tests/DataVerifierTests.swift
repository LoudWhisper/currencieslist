//
//  DataVerifierTests.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 09.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import XCTest

@testable import CurrencyList

// MARK: Class Definition

class DataVerifierTests: XCTestCase {
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
}

// MARK: String Tests

extension DataVerifierTests {
    func testCorrectStringCheckNoThrow() {
        let dataVerifier = DataVerifier()
        let string = UUID().uuidString
        XCTAssertNoThrow(try dataVerifier.string(existAndNotEmptyThrows: string), "data verifier throw on check '\(string)' string")
    }
    
    func testCorrectStringSame() {
        let dataVerifier = DataVerifier()
        let string = UUID().uuidString
        let checkedString = try? dataVerifier.string(existAndNotEmptyThrows: string)
        XCTAssertTrue(string == checkedString)
    }
    
    func testWhitespaceStringCheckNoThrow() {
        let dataVerifier = DataVerifier()
        let string = UUID().uuidString
        let whitespaceString = "  \(string)  "
        XCTAssertNoThrow(try dataVerifier.string(existAndNotEmptyThrows: whitespaceString), "data verifier throw on check '\(whitespaceString)' string")
    }
    
    func testWhitespaceStringCheckSame() {
        let dataVerifier = DataVerifier()
        let string = UUID().uuidString
        let whitespaceString = "  \(string)  "
        let checkedString = try? dataVerifier.string(existAndNotEmptyThrows: whitespaceString)
        XCTAssertTrue(string == checkedString)
    }
    
    func testEmptyStringCheckThrow() {
        let dataVerifier = DataVerifier()
        let string = ""
        XCTAssertThrowsError(try dataVerifier.string(existAndNotEmptyThrows: string))
    }
    
    func testWhitespaceEmptyStringCheckThrow() {
        let dataVerifier = DataVerifier()
        let string = "   "
        XCTAssertThrowsError(try dataVerifier.string(existAndNotEmptyThrows: string))
    }
    
    func testNilStringCheckThrow() {
        let dataVerifier = DataVerifier()
        let string: String? = nil
        XCTAssertThrowsError(try dataVerifier.string(existAndNotEmptyThrows: string))
    }
    
    func testNullStringCheckThrow() {
        let dataVerifier = DataVerifier()
        let string = NSNull()
        XCTAssertThrowsError(try dataVerifier.string(existAndNotEmptyThrows: string))
    }
    
    func testNumberStringCheckNoThrow() {
        let dataVerifier = DataVerifier()
        let string = 42
        XCTAssertNoThrow(try dataVerifier.string(existAndNotEmptyThrows: string), "data verifier throw on check '\(string)' number")
    }
    
    func testLongNumberStringCheckSame() {
        let dataVerifier = DataVerifier()
        let string = UInt64.max
        let checkedString = try? dataVerifier.string(existAndNotEmptyThrows: string)
        XCTAssertTrue("\(string)" == checkedString)
        XCTAssertTrue(string == UInt64(checkedString!))
    }
    
    func testStringsSame() {
        let dataVerifier = DataVerifier()
        let string = UUID().uuidString
        let checkedString = try? dataVerifier.string(existAndNotEmptyThrows: string)
        let checkedStringWithoutThrow = dataVerifier.string(existAndNotEmpty: string)
        XCTAssertTrue(checkedString == checkedStringWithoutThrow)
    }
}

// MARK: URL Tests

extension DataVerifierTests {
    func testCorrectURLCheckNoThrow() {
        let dataVerifier = DataVerifier()
        let string = "https://google.com"
        XCTAssertNoThrow(try dataVerifier.url(existAndNotEmptyThrows: string), "data verifier throw on check '\(string)' url")
    }
    
    func testCorrectURLCheckCorrect() {
        let dataVerifier = DataVerifier()
        let scheme = "https"
        let host = "google.com"
        let string = scheme + "://" + host
        let checkedURL = try? dataVerifier.url(existAndNotEmptyThrows: string)
        if checkedURL == nil || checkedURL?.scheme != scheme || checkedURL?.host != host {
            XCTFail("data verifier wrong url '\(String(describing: checkedURL))' from '\(string)' string")
        }
    }
    
    func testWhitespaceURLCheckNoThrow() {
        let dataVerifier = DataVerifier()
        let scheme = "https"
        let host = "google.com"
        let string = scheme + "://" + host
        let whitespaceString = "  \(string)  "
        XCTAssertNoThrow(try dataVerifier.url(existAndNotEmptyThrows: whitespaceString), "data verifier throw on check '\(string)' url")
    }
    
    func testWhitespaceURLCheckCorrect() {
        let dataVerifier = DataVerifier()
        let scheme = "https"
        let host = "google.com"
        let string = scheme + "://" + host
        let whitespaceString = "  \(string)  "
        let checkedURL = try? dataVerifier.url(existAndNotEmptyThrows: whitespaceString)
        if checkedURL == nil || checkedURL?.scheme != scheme || checkedURL?.host != host {
            XCTFail("data verifier wrong url '\(String(describing: checkedURL))' from '\(string)' url")
        }
    }
    
    func testEmptyURLCheckThrow() {
        let dataVerifier = DataVerifier()
        let string = ""
        XCTAssertThrowsError(try dataVerifier.url(existAndNotEmptyThrows: string))
    }
    
    func testWhitespaceEmptyURLCheckThrow() {
        let dataVerifier = DataVerifier()
        let string = "   "
        XCTAssertThrowsError(try dataVerifier.url(existAndNotEmptyThrows: string))
    }
    
    func testNilURLCheckThrow() {
        let dataVerifier = DataVerifier()
        let string: String? = nil
        XCTAssertThrowsError(try dataVerifier.url(existAndNotEmptyThrows: string))
    }
    
    func testNumberURLCheckThrow() {
        let dataVerifier = DataVerifier()
        let string = 42
        XCTAssertThrowsError(try dataVerifier.url(existAndNotEmptyThrows: string))
    }
    
    func testNullURLCheckThrow() {
        let dataVerifier = DataVerifier()
        let string = NSNull()
        XCTAssertThrowsError(try dataVerifier.url(existAndNotEmptyThrows: string))
    }
    
    func testUrlsSame() {
        let dataVerifier = DataVerifier()
        let string = "https://google.com"
        let checkedString = try? dataVerifier.url(existAndNotEmptyThrows: string)
        let checkedStringWithoutThrow = dataVerifier.url(existAndNotEmpty: string)
        XCTAssertTrue(checkedString == checkedStringWithoutThrow)
    }
}
