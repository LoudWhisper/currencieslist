//
//  ListeningTests.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 09.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import XCTest

@testable import CurrencyList

// MARK: Class Definition

class ListeningTests: XCTestCase {
    private let strongListener = TestListenerClass()
    private let listeners = ListenersContainer<TestListenerProtocol>()
    
    override func setUp() {
        super.setUp()
        
        let weakListenter = TestListenerClass()
        self.listeners.add(weakListener: WeakObject(value: self.strongListener))
        self.listeners.add(weakListener: WeakObject(value: weakListenter))
    }
    
    override func tearDown() {
        super.tearDown()
        
        self.listeners.removeAll()
    }
}

// MARK: Tests

extension ListeningTests {
    func testListeningNotStrong() {
        XCTAssertTrue(self.listeners.count() == 1)
    }
    
    func testListeningNotRepeat() {
        let listeners = ListenersContainer<TestListenerProtocol>()
        
        listeners.add(weakListener: WeakObject(value: self.strongListener))
        listeners.add(weakListener: WeakObject(value: self.strongListener))
        listeners.add(weakListener: WeakObject(value: self.strongListener))
        
        XCTAssertTrue(listeners.count() == 1)
        
        let weakListenter = TestListenerClass()
        listeners.add(weakListener: WeakObject(value: weakListenter))
        listeners.add(weakListener: WeakObject(value: weakListenter))
        listeners.add(weakListener: WeakObject(value: weakListenter))
        
        XCTAssertTrue(listeners.count() == 2)
    }
}

// MARK: Support Classes

@objc protocol TestListenerProtocol: Listener {
    func test()
}

class TestListenerClass: TestListenerProtocol {
    func test() {
        // nothing to do
    }
}
