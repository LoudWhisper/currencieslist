//
//  LocalizerTests.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 09.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import XCTest

@testable import CurrencyList

class LocalizerTests: XCTestCase {
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
}

// MARK: Tests

extension LocalizerTests {
    func testLocalizableValuesExists() {
        let path = Bundle.main.path(forResource: "Localizable", ofType: "plist")
        XCTAssertNotNil(path)
        
        let localizableValues = NSDictionary(contentsOfFile: path!) as? [String : [String : String]]
        XCTAssertNotNil(localizableValues)
    }
    
    func testLocalizerInitialization() {
        XCTAssertNotNil(Localizer.shared.localizableValues)
        XCTAssertNotNil(Localizer().localizableValues)
    }
    
    func testLocalizedValuesStartWithLocalizedKey() {
        let dictionary = Localizer.shared.localizableValues
        for (key, _) in dictionary! {
            XCTAssertTrue(key.hasPrefix("localized_"))
        }
    }
    
    func testLocalizedValuesCorrect() {
        let dictionary = Localizer.shared.localizableValues
        for (_, localizedObject) in dictionary! {
            let value = localizedObject["value"]
            XCTAssertNotNil(value)
            XCTAssertFalse(value!.isEmpty)
            
            let comment = localizedObject["comment"]
            XCTAssertNotNil(comment)
            XCTAssertFalse(comment!.isEmpty)
        }
    }
    
    func testNotLocalizedValuesCorrect() {
        let randomString = UUID().uuidString
        let localizedRandomString = randomString.localized
        
        XCTAssertTrue(localizedRandomString.hasPrefix("**"))
        XCTAssertTrue(localizedRandomString.hasSuffix("**"))
    }
}
