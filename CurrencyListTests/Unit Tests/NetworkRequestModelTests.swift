//
//  NetworkRequestModelTests.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 09.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import XCTest

@testable import CurrencyList

// MARK: Class Definition

class NetworkRequestModelTests: XCTestCase {
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
}

// MARK: Tests

extension NetworkRequestModelTests {
    func testModelNoThrowWithCorrectData() {
        let address = self.correctAddress()
        let parameters: [String : Any] = ["key_1" : "value",
                                          "key_2" : NSNull(),
                                          "key_3" : 42]
        
        XCTAssertNoThrow(try NetworkRequestModel(address: address, parameters: parameters, dataVerifier: DataVerifier()), "network request model throw on initialize with address: '\(address)', parameters: \(parameters)")
    }
    
    func testModelCorrectWithCorrectData() {
        let address = self.correctAddress()
        let parameters: [String : Any] = ["key_1" : "value",
                                          "key_2" : NSNull(),
                                          "key_3" : 42]
        let request = try? NetworkRequestModel(address: address, parameters: parameters, dataVerifier: DataVerifier())
        
        XCTAssertNotNil(request)
        XCTAssertTrue(request!.url.absoluteString == address)
        XCTAssertTrue(request!.parameters!.count == parameters.count)
        
        for key in parameters.keys {
            XCTAssertTrue("\(request!.parameters![key]!)" == "\(parameters[key]!)")
        }
    }
    
    func testModelNoThrowWithoutHeaders() {
        let address = self.correctAddress()
        let parameters: [String : Any] = ["key_1" : "value",
                                          "key_2" : NSNull(),
                                          "key_3" : 42]
        
        XCTAssertNoThrow(try NetworkRequestModel(address: address, parameters: parameters, dataVerifier: DataVerifier()), "network request model throw on initialize with address: '\(address)', parameters: \(parameters)")
    }
    
    func testModelNoThrowWithoutParameters() {
        let address = self.correctAddress()
        
        XCTAssertNoThrow(try NetworkRequestModel(address: address, parameters: nil, dataVerifier: DataVerifier()), "network request model throw on initialize with address: '\(address)'")
    }
    
    func testModelThrowWithUncorrectData() {
        let address = self.uncorrectAddress()
        
        XCTAssertThrowsError(try NetworkRequestModel(address: address, parameters: nil, dataVerifier: DataVerifier()))
    }
}

// MARK: Support Methods

private extension NetworkRequestModelTests {
    func correctAddress() -> String {
        return "https://google.com"
    }
    
    func uncorrectAddress() -> String {
        return UUID().uuidString
    }
}
