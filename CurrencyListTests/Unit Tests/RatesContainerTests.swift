//
//  RatesContainerTests.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 09.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import XCTest

@testable import CurrencyList

// MARK: Class Definition

class RatesContainerTests: XCTestCase {
    
    private struct Keys {
        static let currency = "base"
        static let rates = "rates"
    }
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
}

// MARK: Tests

extension RatesContainerTests {
    func testModelNoThrow() {
        let response: [String : Any] = self.correctResponse()
        XCTAssertNoThrow(try RatesContainerModel(response: response, dataVerifier: DataVerifier()))
    }
    
    func testModelCorrect() {
        let response: [String : Any] = self.correctResponse()
        let ratesContainer = try? RatesContainerModel(response: response, dataVerifier: DataVerifier())
        
        XCTAssertNotNil(ratesContainer)
        XCTAssertTrue(ratesContainer?.currencyWithAmount.currency == self.defaultCurrency())
        XCTAssertTrue(ratesContainer?.currencyWithAmount.amount == nil)
        XCTAssertTrue(ratesContainer?.currencies.contains(self.defaultCurrency()) == true)
        XCTAssertTrue(ratesContainer?.currencies.contains(self.defaultRatesCurrency()) == true)
        XCTAssertTrue(ratesContainer?.rateForCurrency[self.defaultRatesCurrency().rawValue] != nil)
        XCTAssertTrue(ratesContainer?.rateForCurrency[self.defaultRatesCurrency().rawValue]?.currency == self.defaultRatesCurrency())
        XCTAssertTrue(ratesContainer?.rateForCurrency[self.defaultRatesCurrency().rawValue]?.value == self.defaultRatesValue())
    }
    
    func testModelThrowWithoutBaseCurrency() {
        var response: [String : Any] = self.correctResponse()
        response[Keys.currency] = NSNull()
        XCTAssertThrowsError(try RatesContainerModel(response: response, dataVerifier: DataVerifier()))
    }
    
    func testModelThrowWithWrongBaseCurrency() {
        var response: [String : Any] = self.correctResponse()
        response[Keys.currency] = UUID().uuidString
        XCTAssertThrowsError(try RatesContainerModel(response: response, dataVerifier: DataVerifier()))
    }
    
    func testModelThrowWithUndefinedBaseCurrency() {
        var response: [String : Any] = self.correctResponse()
        response[Keys.currency] = self.undefinedCurrency().rawValue
        XCTAssertThrowsError(try RatesContainerModel(response: response, dataVerifier: DataVerifier()))
    }
    
    func testModelThrowWithoutRates() {
        var response: [String : Any] = self.correctResponse()
        response[Keys.rates] = NSNull()
        XCTAssertThrowsError(try RatesContainerModel(response: response, dataVerifier: DataVerifier()))
    }
    
    func testModelThrowWithEmptyRates() {
        var response: [String : Any] = self.correctResponse()
        response[Keys.rates] = []
        XCTAssertThrowsError(try RatesContainerModel(response: response, dataVerifier: DataVerifier()))
    }
    
    func testDefaultModelNoThrow() {
        XCTAssertNoThrow(try RatesContainerModel(currency: self.defaultCurrency(), dataVerifier: DataVerifier()))
    }
    
    func testDefaultModelCorrect() {
        let ratesContainer = try? RatesContainerModel(currency: self.defaultCurrency(), dataVerifier: DataVerifier())
        
        XCTAssertNotNil(ratesContainer)
        XCTAssertTrue(ratesContainer?.currencyWithAmount.currency == self.defaultCurrency())
        XCTAssertTrue(ratesContainer?.currencyWithAmount.amount == nil)
        XCTAssertTrue(ratesContainer?.currencies.contains(self.defaultCurrency()) == true)
    }
    
    func testDefaultModelThrowWithUndefinedBaseCurrency() {
        XCTAssertThrowsError(try RatesContainerModel(currency: self.undefinedCurrency(), dataVerifier: DataVerifier()))
    }
}

// MARK: Support Methods

private extension RatesContainerTests {
    func correctResponse() -> [String : Any] {
        let response: [String : Any] = [Keys.currency : self.defaultCurrency().rawValue,
                                        Keys.rates : [self.defaultRatesCurrency().rawValue : self.defaultRatesValue()]]
        return response
    }
    
    func defaultCurrency() -> Currency {
        return .USD
    }
    
    func undefinedCurrency() -> Currency {
        return .undefined
    }
    
    func defaultRatesCurrency() -> Currency {
        return .EUR
    }
    
    func defaultRatesValue() -> String {
        return "1.1"
    }
}
