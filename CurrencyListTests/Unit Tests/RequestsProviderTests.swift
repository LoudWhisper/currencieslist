//
//  RequestsProviderTests.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 09.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import XCTest

@testable import CurrencyList

// MARK: Class Definition

class RequestsProviderTests: XCTestCase {
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
}

// MARK: Tests

extension RequestsProviderTests {
    func testRatesPathsCorrect() {
        let requestsProvider = self.requestsProvider()
        
        let pathType = AddressesProvider.Paths.rates(pathContainer: .list)
        self.check(pathType: pathType, withRequestsProvider: requestsProvider)
        
        let ratesPath = self.anyRatesPath()
        switch ratesPath {
        case .list:
            // tested
            break
        }
    }
}

// MARK: Support Methods

private extension RequestsProviderTests {
    func requestsProvider() -> RequestsProvider {
        let dataVerifier = DataVerifier()
        let addressesProvider = AddressesProvider(targetValuesProvider: TargetValuesProvider(dataVerifier: dataVerifier))
        let provider = RequestsProvider(addressesProvider: addressesProvider, dataVerifier: dataVerifier)
        return provider
    }
    
    func accessToken() -> String {
        return "accessToken"
    }
    
    func check(pathType: AddressesProvider.Paths, withRequestsProvider requestsProvider: RequestsProvider) {
        let request = requestsProvider.request(withAddress: pathType, parameters: nil)
        XCTAssertNotNil(request)
        XCTAssertNotNil(request!.url)
        XCTAssertNotNil(request!.url.scheme)
        XCTAssertNotNil(request!.url.host)
        
        let parameters: [String : String] = ["first" : "value_1",
                                             "second" : "value_2"]
        let requestWithParameters = requestsProvider.request(withAddress: pathType, parameters: parameters)
        XCTAssertNotNil(requestWithParameters)
        XCTAssertNotNil(requestWithParameters!.url)
        XCTAssertNotNil(requestWithParameters!.url.scheme)
        XCTAssertNotNil(requestWithParameters!.url.host)
        XCTAssertNotNil(requestWithParameters!.parameters)
        XCTAssertTrue(requestWithParameters!.parameters!["first"] as? String == parameters["first"])
        XCTAssertTrue(requestWithParameters!.parameters!["second"] as? String == parameters["second"])
    }
    
    func anyRatesPath() -> AddressesProvider.RatesPath {
        return AddressesProvider.RatesPath.list
    }
}
