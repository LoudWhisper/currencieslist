//
//  TargetValuesProviderTests.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 09.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import XCTest

@testable import CurrencyList

// MARK: Class Definition

class TargetValuesProviderTests: XCTestCase {
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
}

// MARK: Tests

extension TargetValuesProviderTests {
    func testTargetValuesExists() {
        let targetValues = Bundle.main.infoDictionary?["GDTargetDefinedValues"] as? [String : Any]
        XCTAssertNotNil(targetValues)
    }
    
    func textTargetValuesInitialization() {
        let provider = self.provider()
        XCTAssertNotNil(provider)
    }
    
    func testTargetValuesCorrectAndTested() {
        var valuesTested = 0
        let provider = self.provider()
        
        XCTAssertNotNil(provider.serverAddress())
        valuesTested += 1
        
        if provider.valuesCount() != valuesTested {
            XCTFail("not all values tested")
        }
    }
    
    func testTargetServerAddressCorrect() {
        let provider = self.provider()
        let address = provider.serverAddress()
        let url = URL(string: address)
        XCTAssertNotNil(address)
        XCTAssertNotNil(url)
        XCTAssertNotNil(url!.scheme)
        XCTAssertNotNil(url!.host)
        
        if !address.hasPrefix("https://") {
            XCTFail("\(address) must start with 'https://'")
        }
        
        if !address.hasSuffix("/") {
            XCTFail("\(address) must end with '/' to correctly add path components")
        }
    }
}

// MARK: Support Methods

private extension TargetValuesProviderTests {
    func provider() -> TargetValuesProvider {
        let provider = TargetValuesProvider(dataVerifier: DataVerifier())
        return provider
    }
    
    func check(address: String) {
        let url = URL(string: address)
        XCTAssertNotNil(address)
        XCTAssertNotNil(url)
        XCTAssertNotNil(url!.scheme)
        XCTAssertNotNil(url!.host)
        
        if !address.hasPrefix("https://") && !address.hasPrefix("http://") {
            XCTFail("\(address) must start with 'https://' or 'http://'")
        }
    }
}
