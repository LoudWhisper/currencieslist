//
//  ScreensAssembly.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import UIKit

// MARK: Class Definition

class ScreensAssembly {
    
    // MARK: Private Properties
    
    private var servicesAssembly: ServicesAssembly!
    
    // MARK: Init Methods & Superclass Overriders
    
    /// Initializes assembly with parameters.
    ///
    /// - Parameter servicesAssembly: Services assembly.
    required init(withServicesAssembly servicesAssembly: ServicesAssembly) {
        self.servicesAssembly = servicesAssembly
    }
}

// MARK: Public Methods

extension ScreensAssembly {
    /// Initializes and configures view controller with type.
    ///
    /// - Parameter screens: Screens type.
    /// - Returns: Configured view controller.
    func viewController(withType screens: ScreensProvider.Screens) -> UIViewController {
        var viewController: UIViewController!
        do {
            viewController = try self.screen(withType: screens)
        } catch let error {
            // TODO: Report an error in the analytics services. Remove print.
            print(error)
        }
        return viewController
    }
    
    /// Initializes and configures navigation controller with type.
    ///
    /// - Parameter screens: Screens type.
    /// - Returns: Configured navigation controller.
    func navigationController(withType screens: ScreensProvider.Screens) -> UINavigationController {
        var viewController: UINavigationController!
        do {
            viewController = try self.screen(withType: screens) as? UINavigationController
        } catch let error {
            // TODO: Report an error in the analytics services. Remove print.
            print(error)
        }
        return viewController
    }
    
    /// Initializes and configures tab bar controller with type.
    ///
    /// - Parameter screens: Screens type.
    /// - Returns: Configured tab bar controller.
    func tabBarController(withType screens: ScreensProvider.Screens) -> UITabBarController {
        var viewController: UITabBarController!
        do {
            viewController = try self.screen(withType: screens) as? UITabBarController
        } catch let error {
            // TODO: Report an error in the analytics services. Remove print.
            print(error)
        }
        return viewController
    }
}

// MARK: Support Methods

private extension ScreensAssembly {
    func screen(withType screens: ScreensProvider.Screens) throws -> UIViewController {
        switch screens {
        case .rates(let screenContainer):
            switch screenContainer {
            case .list(let router):
                let viewController = try screens.viewController(withType: RatesListViewController.self)
                let viewModel = RatesListViewModel(withRouter: router, output: viewController)
                let dataModel = RatesListDataModel(output: viewModel, networkService: self.servicesAssembly.ratesNetworkService, ratesService: self.servicesAssembly.ratesService)
                viewController.viewModel = viewModel.withDataModel(dataModel)
                return viewController
            }
        case .common(let screenContainer):
            switch screenContainer {
            case .navigation:
                let viewController = try screens.viewController(withType: UINavigationController.self)
                return viewController
            }
        }
    }
}
