//
//  ServicesAssembly.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import Foundation

// MARK: Class Definition

class ServicesAssembly {
    
    // MARK: Lazy Public Properties
    
    private(set) lazy var dataVerifier = DataVerifier()
    private(set) lazy var targetValuesProvider = TargetValuesProvider(dataVerifier: self.dataVerifier)
    private(set) lazy var addressesProvider = AddressesProvider(targetValuesProvider: self.targetValuesProvider)
    private(set) lazy var requestsProvider = RequestsProvider(addressesProvider: self.addressesProvider, dataVerifier: self.dataVerifier)
    private(set) lazy var networkRequestsHandler = NetworkRequestsHandler(requestsProvider: self.requestsProvider)
    
    // MARK: Weak Public Properties
    
    var ratesNetworkService: RatesNetworkService! {
        get {
            return self.setupRatesNetworkService()
        }
    }
    
    var ratesService: RatesService! {
        get {
            return self.setupRatesService()
        }
    }
    
    // MARK: Private Properties
    
    private weak var weakRatesNetworkService: RatesNetworkService?
    private weak var weakRatesService: RatesService?
}

// MARK: Support Methods

private extension ServicesAssembly {
    func setupRatesNetworkService() -> RatesNetworkService? {
        var service = self.weakRatesNetworkService
        if service == nil {
            service = RatesNetworkService(networkRequestsHandler: self.networkRequestsHandler, targetValuesProvider: self.targetValuesProvider)
            self.weakRatesNetworkService = service
        }
        return service
    }
    
    func setupRatesService() -> RatesService? {
        var service = self.weakRatesService
        if service == nil {
            service = RatesService(dataVerifier: self.dataVerifier)
            self.weakRatesService = service
        }
        return service
    }
}
