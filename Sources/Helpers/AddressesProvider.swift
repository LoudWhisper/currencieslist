//
//  AddressesProvider.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import Foundation

private protocol PathContainer {
    var path: String { get }
}

// MARK: Class Definition

class AddressesProvider {
    
    // MARK: Enums
    
    enum Paths {
        case rates(pathContainer: RatesPath)
    }
    
    enum RatesPath {
        case list
    }
    
    // MARK: Private Properties
    
    private var targetValuesProvider: TargetValuesProvider!
    
    // MARK: Init Methods & Superclass Overriders
    
    /// Initializes target values provider with parameters.
    ///
    /// - Parameters:
    ///   - targetValuesProvider: Target values provider.
    required init(targetValuesProvider: TargetValuesProvider) {
        self.targetValuesProvider = targetValuesProvider
    }
}

// MARK: Public Methods

extension AddressesProvider {
    /// Creates an address combining the host and path.
    ///
    /// - Parameter type: Path type.
    /// - Returns: Address string.
    func address(forPathType type: Paths) -> String {
        let host = self.targetValuesProvider.serverAddress()
        let path = type.path
        return host + path
    }
}

// MARK: Paths

extension AddressesProvider.Paths: PathContainer {
    var path: String {
        switch self {
        case .rates(let pathContainer):
            return pathContainer.path
        }
    }
}

// MARK: RatesPath

extension AddressesProvider.RatesPath: PathContainer {
    var path: String {
        switch self {
        case .list:
            return "latest"
        }
    }
}
