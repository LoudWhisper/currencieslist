//
//  DataVerifier.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import Foundation

// MARK: Class Definition

class DataVerifier {
    
    // MARK: Enums
    
    enum Errors: Error {
        case data(value: Any?, result: Any?)
    }
}

// MARK: Public Methods

extension DataVerifier {
    /// Checks that the string exists and not empty.
    /// Converts a value to a string other than nil and null.
    ///
    /// - Parameters:
    ///   - value: Value to check.
    /// - Returns: Not empty string from the value without whitespaces.
    /// - Throws: An error if the string does not exist or is empty.
    func string(existAndNotEmptyThrows value: Any?) throws -> String {
        let notEmptyString: String = (value != nil ? "\(value!)" : "")!
        if value == nil || value is NSNull || notEmptyString.isEmpty {
            throw(Errors.data(value: value, result: notEmptyString))
        }
        
        let clearString = notEmptyString.trimmingCharacters(in: .whitespacesAndNewlines)
        if clearString.isEmpty {
            throw(Errors.data(value: value, result: clearString))
        }
        
        return clearString
    }
    
    /// Checks that the string exists and not empty.
    /// Converts a value to a string other than nil and null.
    ///
    /// - Parameters:
    ///   - value: Value to check.
    /// - Returns: Not empty string from the value without whitespaces or nil.
    func string(existAndNotEmpty value: Any?) -> String? {
        guard let clearString = try? self.string(existAndNotEmptyThrows: value) else {
            return nil
        }
        return clearString
    }
    
    /// Checks that the url exists and not empty.
    /// Converts a value to a url other than nil and null.
    ///
    /// - Parameters:
    ///   - value: Value to check.
    /// - Returns: Not empty url from the value without whitespaces.
    /// - Throws: An error if the url does not exist or is empty.
    func url(existAndNotEmptyThrows value: Any?) throws -> URL {
        let notEmptyString: String = (value != nil ? "\(value!)" : "")!
        if value == nil || value is NSNull || notEmptyString.isEmpty {
            throw(Errors.data(value: value, result: notEmptyString))
        }
        
        let clearString = notEmptyString.trimmingCharacters(in: .whitespacesAndNewlines)
        if clearString.isEmpty {
            throw(Errors.data(value: value, result: clearString))
        }
        
        guard let url = URL(string: clearString), let _ = url.scheme, let _ = url.host else {
            throw(Errors.data(value: value, result: clearString))
        }
        
        return url
    }
    
    /// Checks that the url exists and not empty.
    /// Converts a value to a url other than nil and null.
    ///
    /// - Parameters:
    ///   - value: Value to check.
    /// - Returns: Not empty url from the value without whitespaces or nil.
    func url(existAndNotEmpty value: Any?) -> URL? {
        guard let url = try? self.url(existAndNotEmptyThrows: value) else {
            return nil
        }
        return url
    }
}
