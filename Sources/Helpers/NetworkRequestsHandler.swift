//
//  NetworkRequestsHandler.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import Foundation

import Alamofire

// MARK: Class Definition

class NetworkRequestsHandler {
    
    typealias FailureData = (accessDenied: Bool, requestTimedOut: Bool, networkNotReachable: Bool, message: String)
    
    typealias SuccessBlock = ((_ response: [String : Any]?) -> ())
    typealias FailureBlock = ((_ failureData: FailureData) -> ())
    typealias CancellationBlock = (() -> ())
    typealias SessionTaskBlock = ((_ task: URLSessionTask?) -> ())
    
    // MARK: Structs
    
    private struct Keys {
        static let message = "message"
    }
    
    // MARK: Private Properties
    
    private var requestsProvider: RequestsProvider!
    private var sessionManager: SessionManager!
    private var activeRequests = 0
    
    // MARK: Init Methods & Superclass Overriders
    
    /// Initializes network service with parameters.
    ///
    /// - Parameters:
    ///   - requestsProvider: Requests provider.
    required init(requestsProvider: RequestsProvider) {
        self.requestsProvider = requestsProvider
        
        self.configureSessionManager()
    }
    
    deinit {
        self.sessionManager.session.invalidateAndCancel()
    }
}

// MARK: Public Methods

extension NetworkRequestsHandler {
    /// Runs network request with GET method.
    ///
    /// - Parameters:
    ///   - address: Address from addresses provider.
    ///   - parameters: Request parameters.
    ///   - onSuccess: Block. Called when the request succeeds.
    ///   - onFailure: Block. Called when the request fails.
    ///   - onCancellation: Block. Called when the request cancels.
    ///   - onSessionTask: Block. Called when session task created.
    func get(_ address: AddressesProvider.Paths, parameters: [String : Any]?, onSuccess: SuccessBlock?, onFailure: FailureBlock?, onCancellation: CancellationBlock?, onSessionTask: SessionTaskBlock?) {
        guard let request = self.requestsProvider.request(withAddress: address, parameters: parameters) else {
            return
        }
        
        self.createAndRunRequestWith(request: request, method: .get, onSuccess: onSuccess, onFailure: onFailure, onCancellation: onCancellation, onSessionTask: onSessionTask)
    }
    
    /// Runs network request with POST method.
    ///
    /// - Parameters:
    ///   - address: Address from addresses provider.
    ///   - parameters: Request parameters.
    ///   - onSuccess: Block. Called when the request succeeds.
    ///   - onFailure: Block. Called when the request fails.
    ///   - onCancellation: Block. Called when the request cancels.
    ///   - onSessionTask: Block. Called when session task created.
    func post(_ address: AddressesProvider.Paths, parameters: [String : Any]?, onSuccess: SuccessBlock?, onFailure: FailureBlock?, onCancellation: CancellationBlock?, onSessionTask: SessionTaskBlock?) {
        guard let request = self.requestsProvider.request(withAddress: address, parameters: parameters) else {
            return
        }
        
        self.createAndRunRequestWith(request: request, method: .post, onSuccess: onSuccess, onFailure: onFailure, onCancellation: onCancellation, onSessionTask: onSessionTask)
    }
}

// MARK: Configurations

private extension NetworkRequestsHandler {
    func configureSessionManager() {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        configuration.timeoutIntervalForRequest = 30.0
        
        self.sessionManager = Alamofire.SessionManager(configuration: configuration)
    }
}

// MARK: Request Methods

private extension NetworkRequestsHandler {
    func createAndRunRequestWith(request: NetworkRequestModel, method: HTTPMethod, onSuccess: SuccessBlock?, onFailure: FailureBlock?, onCancellation: CancellationBlock?, onSessionTask: SessionTaskBlock?) {
        guard let dataRequest = self.run(request: request, method: method, onSuccess: onSuccess, onFailure: onFailure, onCancellation: onCancellation, onSessionTask: onSessionTask) else {
            return
        }
        self.perform(onSessionTask, sessionTask: dataRequest.task)
    }
    
    func run(request: NetworkRequestModel, method: HTTPMethod, onSuccess: SuccessBlock?, onFailure: FailureBlock?, onCancellation: CancellationBlock?, onSessionTask: SessionTaskBlock?) -> DataRequest? {
        #if DEBUG
        print("\n[NETWORK HANDLER] SEND \(method.rawValue) \(String(describing: request))\n")
        #endif
        
        self.increaseRequestsCount()
        
        let encoding: ParameterEncoding = (method == .post ? JSONEncoding.default : URLEncoding.default)
        let request = self.sessionManager.request(request.url, method: method, parameters: request.parameters, encoding: encoding, headers: nil)
            .validate()
            .responseJSON { [weak self] (response) in
                self?.request(request, method: method, finishedWithResponse: response, onSuccess: onSuccess, onFailure: onFailure, onCancellation: onCancellation, onSessionTask: onSessionTask)
                self?.decreaseRequestsCount()
        }
        return request
    }
    
    func request(_ request: NetworkRequestModel, method: HTTPMethod, finishedWithResponse response: DataResponse<Any>, onSuccess: SuccessBlock?, onFailure: FailureBlock?, onCancellation: CancellationBlock?, onSessionTask: SessionTaskBlock?) {
        #if DEBUG
        print("\n[NETWORK HANDLER] RECEIVE \(method.rawValue) \(String(describing: request))\n\(response.result.description)\n")
        #endif
        
        let error = (response.error as NSError?)
        let accessDenied = (response.response?.statusCode == 403 || response.response?.statusCode == 401)
        let requestCancelled = (error?.code == NSURLErrorCancelled)
        let requestTimedOut = (error?.code == NSURLErrorTimedOut || response.response?.statusCode == 0)
        let networkNotReachable = (error?.code == NSURLErrorNetworkConnectionLost || error?.code == NSURLErrorNotConnectedToInternet)
        
        if error != nil || accessDenied || requestCancelled || requestTimedOut || networkNotReachable {
            if requestCancelled {
                self.perform(onCancellation)
            } else {
                self.perform(onFailure, withAccessDenied: accessDenied, requestTimedOut: requestTimedOut, networkNotReachable: networkNotReachable, error: error, response: response)
            }
        } else {
            self.perform(onSuccess, withResponse: (response.value as? [String : Any]))
        }
    }
}

// MARK: Perform completion blocks

private extension NetworkRequestsHandler {
    func perform(_ block: SuccessBlock?, withResponse response: [String : Any]?) {
        guard let codeToExecute = block else {
            return
        }
        codeToExecute(response)
    }
    
    func perform(_ block: FailureBlock?, withAccessDenied accessDenied: Bool, requestTimedOut: Bool, networkNotReachable: Bool, error: Error?, response: DataResponse<Any>) {
        guard let codeToExecute = block else {
            return
        }
        
        let message = self.message(withAccessDenied: accessDenied, requestTimedOut: requestTimedOut, networkNotReachable: networkNotReachable, error: error, response: response)
        codeToExecute((accessDenied: accessDenied, requestTimedOut: requestTimedOut, networkNotReachable: networkNotReachable, message: message))
    }
    
    func perform(_ block: CancellationBlock?) {
        guard let codeToExecute = block else {
            return
        }
        codeToExecute()
    }
    
    func perform(_ block: SessionTaskBlock?, sessionTask: URLSessionTask?) {
        guard let codeToExecute = block else {
            return
        }
        codeToExecute(sessionTask)
    }
}

// MARK: Network Activity Indicator

private extension NetworkRequestsHandler {
    func increaseRequestsCount() {
        DispatchQueue.main.async {
            self.activeRequests += 1
            UIApplication.shared.isNetworkActivityIndicatorVisible = (self.activeRequests > 0)
        }
    }
    
    func decreaseRequestsCount() {
        DispatchQueue.main.async {
            self.activeRequests -= 1
            UIApplication.shared.isNetworkActivityIndicatorVisible = (self.activeRequests > 0)
        }
    }
}

// MARK: Support Methods

private extension NetworkRequestsHandler {
    func errorResponseAsDictionary(_ response: DataResponse<Any>?) -> [String : Any]? {
        guard let data = response?.data, let errorResponse = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : Any] else {
            return nil
        }
        return errorResponse
    }
    
    func message(withAccessDenied accessDenied: Bool, requestTimedOut: Bool, networkNotReachable: Bool, error: Error?, response: DataResponse<Any>?) -> String {
        if requestTimedOut {
            return "localized_networkTimedOut".localized
        } else if networkNotReachable {
            return "localized_networkNotReachable".localized
        }
        
        if let errorResponse = self.errorResponseAsDictionary(response), let string = errorResponse[Keys.message] as? String, !string.isEmpty {
            return string
        } else if let data = response?.data, let string = String(data: data, encoding: .utf8), !string.isEmpty {
            return string
        }
        
        if let string = error?.localizedDescription {
            return string
        } else {
            return "localized_networkUnknown".localized
        }
    }
}
