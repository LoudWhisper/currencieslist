//
//  RequestsProvider.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import Foundation

// MARK: Class Definition

class RequestsProvider {
    
    // MARK: Private Properties
    
    private var addressesProvider: AddressesProvider!
    private var dataVerifier: DataVerifier!
    
    // MARK: Init Methods & Superclass Overriders
    
    /// Initializes requests provider with parameters.
    ///
    /// - Parameters:
    ///   - addressesProvider: Addresses provider.
    ///   - dataVerifier: Data verifier.
    required init(addressesProvider: AddressesProvider, dataVerifier: DataVerifier) {
        self.addressesProvider = addressesProvider
        self.dataVerifier = dataVerifier
    }
}

// MARK: Public Methods

extension RequestsProvider {
    /// Creates network reqeust with parameters.
    ///
    /// - Parameters:
    ///   - address: Request address.
    ///   - parameters: Request parameters.
    /// - Returns: Configured network request.
    func request(withAddress address: AddressesProvider.Paths, parameters: [String : Any]?) -> NetworkRequestModel? {
        let fullAddress = self.addressesProvider.address(forPathType: address)
        
        var request: NetworkRequestModel?
        do {
            request = try NetworkRequestModel(address: fullAddress, parameters: parameters, dataVerifier: self.dataVerifier)
        } catch let error {
            // TODO: Report an error in the analytics services. Remove print.
            print(error)
        }
        return request
    }
}
