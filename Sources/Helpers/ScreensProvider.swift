//
//  ScreensProvider.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import UIKit

private protocol ScreenContainer {
    var viewControllerIdentifier: String { get }
}

// MARK: Class Definition

class ScreensProvider {
    
    // MARK: Enums
    
    enum Errors: Error {
        case invalidStoryboard(identifier: String?)
        case invalidViewController(storyboardName: String?, viewControllerIdentifier: String?, viewControllerClass: String?)
    }
    
    enum Screens {
        case rates(screenContainer: RatesScreen)
        case common(screenContainer: CommonScreen)
    }
    
    enum RatesScreen {
        case list(router: RatesRoutesProviderable)
    }
    
    enum CommonScreen {
        case navigation
    }
}

// MARK: Errors

extension ScreensProvider.Errors {
    static func parameters(fromError error: Error) -> (storyboardName: String?, viewControllerIdentifier: String?, viewControllerClass: String?)? {
        guard let customError = error as? ScreensProvider.Errors else {
            return nil
        }
        
        switch customError {
        case .invalidStoryboard(let identifier):
            return (storyboardName: identifier, viewControllerIdentifier: nil, viewControllerClass: nil)
        case .invalidViewController(let storyboardName, let viewControllerIdentifier, let viewControllerClass):
            return (storyboardName: storyboardName, viewControllerIdentifier: viewControllerIdentifier, viewControllerClass: viewControllerClass)
        }
    }
}

// MARK: Screens

extension ScreensProvider.Screens {
    var storyboardIdentifier: String {
        switch self {
        case .rates(_):
            return "Rates"
        case .common(_):
            return "Common"
        }
    }
    
    var viewControllerIdentifier: String {
        switch self {
        case .rates(let screenContainer):
            return screenContainer.viewControllerIdentifier
        case .common(let screenContainer):
            return screenContainer.viewControllerIdentifier
        }
    }
    
    var storyboard: UIStoryboard? {
        return UIStoryboard(name: self.storyboardIdentifier, bundle: nil)
    }
    
    func viewController<T: UIViewController>(withType type: T.Type) throws -> T {
        guard let storyboard = self.storyboard else {
            throw(ScreensProvider.Errors.invalidStoryboard(identifier: self.storyboardIdentifier))
        }
        
        guard let viewController = storyboard.instantiateViewController(withIdentifier: self.viewControllerIdentifier) as? T else {
            throw(ScreensProvider.Errors.invalidViewController(storyboardName: self.storyboardIdentifier, viewControllerIdentifier: self.viewControllerIdentifier, viewControllerClass: String(describing: T.self)))
        }
        
        return viewController
    }
}

// MARK: RatesScreen

extension ScreensProvider.RatesScreen: ScreenContainer {
    var viewControllerIdentifier: String {
        switch self {
        case .list(_):
            return String(describing: RatesListViewController.self)
        }
    }
}

// MARK: CommonScreen

extension ScreensProvider.CommonScreen: ScreenContainer {
    var viewControllerIdentifier: String {
        switch self {
        case .navigation:
            return String(describing: UINavigationController.self)
        }
    }
}
