//
//  TargetValuesProvider.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import Foundation

// MARK: Class Definition

class TargetValuesProvider {
    
    // MARK: Structs
    
    private struct Keys {
        static let targetValuesDictionary = "GDTargetDefinedValues"
        
        static let serverAddress = "api_address"
    }
    
    // MARK: Private Properties
    
    private var dataVerifier: DataVerifier!
    
    private var targetValues: [String : Any]!
    
    // MARK: Init Methods & Superclass Overriders
    
    /// Initializes target values provider with parameters.
    ///
    /// - Parameters:
    ///   - dataVerifier: Data verifier.
    required init(dataVerifier: DataVerifier) {
        self.dataVerifier = dataVerifier
        
        guard let targetValues = Bundle.main.infoDictionary?[Keys.targetValuesDictionary] as? [String : Any] else {
            fatalError("Target values file not found.")
        }
        self.targetValues = targetValues
    }
}

// MARK: Public Methods

extension TargetValuesProvider {
    /// Searches for target values from plist and gets count.
    ///
    /// - Returns: Target values count.
    func valuesCount() -> Int {
        return self.targetValues.count
    }
    
    /// Searches for server address from target values from plist.
    ///
    /// - Returns: Server address.
    func serverAddress() -> String {
        return self.value(forKey: Keys.serverAddress)
    }
}

// MARK: Support Methods

private extension TargetValuesProvider {
    func value(forKey key: String) -> String {
        var string: String!
        do {
            string = try self.dataVerifier.string(existAndNotEmptyThrows: self.targetValues[key])
        } catch let error {
            // TODO: Report an error in the analytics services. Remove print.
            print(error)
        }
        return string
    }
    
    func valueOrNil(forKey key: String) -> String? {
        return self.dataVerifier.string(existAndNotEmpty: self.targetValues[key])
    }
}
