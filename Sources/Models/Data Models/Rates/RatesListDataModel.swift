//
//  RatesListDataModel.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import Foundation

// MARK: Class Definition

protocol RatesListDataOutput: class {
    func applyState(_ state: RatesListViewModel.State)
}

protocol RatesListDataInput {
    func loadRates(forCurrency currency: Currency?)
    func changeAmount(_ amount: String?)
}

// MARK: Class Definition

class RatesListDataModel {
    
    // MARK: Private Properties
    
    private weak var output: RatesListDataOutput?
    
    private var networkService: RatesNetworkService!
    private var ratesService: RatesService!
    
    private var currency: Currency?
    
    // MARK: Init Methods & Superclass Overriders
    
    /// Initializes data model with parameters.
    ///
    /// - Parameters:
    ///   - output: Data model output.
    ///   - networkService: Rates network service.
    ///   - ratesService: Rates service.
    required init(output: RatesListDataOutput, networkService: RatesNetworkService, ratesService: RatesService) {
        self.output = output
        self.networkService = networkService
        self.ratesService = ratesService
    }
}

// MARK: RatesListDataInput

extension RatesListDataModel: RatesListDataInput {
    func loadRates(forCurrency currency: Currency?) {
        let ratesForCurrency = self.ratesService.ratesForCurrency(currency)
        
        self.currency = ratesForCurrency.currencyWithAmount.currency
        self.ratesService.updateCurrency(ratesForCurrency.currencyWithAmount.currency)
        self.changeOutputState(.suspense(currencyWithAmount: ratesForCurrency.currencyWithAmount, rates: ratesForCurrency.rates))
        self.loadRatesList(currency: ratesForCurrency.currencyWithAmount.currency)
    }
    
    func changeAmount(_ amount: String?) {
        self.ratesService.updateAmount(amount)
    }
}

// MARK: Network Requests

private extension RatesListDataModel {
    func loadRatesList(currency: Currency) {
        let requestDate = Date()
        self.networkService.loadRatesListWith(currency: currency, onSuccess: { [weak self] (response) in
            self?.ratesListDidLoadWith(serverResponse: response, forCurrency: currency, requestDate: requestDate)
            }, onFailure: { [weak self] (failure) in
                self?.ratesListDidFailToLoad(forCurrency: currency)
        }, onCancellation: nil, onSessionTask: nil)
    }
}

// MARK: Process Response

private extension RatesListDataModel {
    func ratesListDidLoadWith(serverResponse: [String : Any]?, forCurrency currency: Currency, requestDate: Date) {
        if self.currency == nil || self.currency != currency {
            return
        }
        
        if let response = serverResponse {
            self.ratesService.ratesListDidLoad(withResponse: response)
        }
        
        let timeIntervalSinceRequest = requestDate.timeIntervalSinceNow
        let waitInterval = 1.0 + timeIntervalSinceRequest
        if waitInterval > 0.0 {
            DispatchQueue.global().asyncAfter(deadline: .now() + waitInterval) { [weak self] in
                self?.loadRatesList(currency: currency)
            }
        } else {
            self.loadRatesList(currency: currency)
        }
        
    }
    
    func ratesListDidFailToLoad(forCurrency currency: Currency) {
        if self.currency == nil || self.currency != currency {
            return
        }
        self.loadRatesList(currency: currency)
    }
}

// MARK: Support Methods

private extension RatesListDataModel {
    func changeOutputState(_ state: RatesListViewModel.State) {
        DispatchQueue.main.async {
            self.output?.applyState(state)
        }
    }
}
