//
//  Currency.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import UIKit

// MARK: Enum Definition

enum Currency: String {
    case EUR
    case AUD
    case BGN
    case BRL
    case CAD
    case CHF
    case CNY
    case CZK
    case DKK
    case GBP
    case HKD
    case HRK
    case HUF
    case IDR
    case ILS
    case INR
    case ISK
    case JPY
    case KRW
    case MXN
    case MYR
    case NOK
    case NZD
    case PHP
    case PLN
    case RON
    case RUB
    case SEK
    case SGD
    case THB
    case TRY
    case USD
    case ZAR
    case undefined
}

// MARK: Public Methods

extension Currency {
    static func values() -> [Currency] {
        let values: [Currency] = [.EUR, .AUD, .BGN, .BRL, .CAD, .CHF, .CNY, .CZK, .DKK, .GBP, .HKD, .HRK, .HUF, .IDR, .ILS, .INR, .ISK, .JPY, .KRW, .MXN, .MYR, .NOK, .NZD, .PHP, .PLN, .RON, .RUB, .SEK, .SGD, .THB, .TRY, .USD, .ZAR]
        return values
    }
    
    static func count() -> Int {
        return 33
    }
    
    static func valueToType(_ value: Any?) -> Currency {
        if let string = value as? String, let type = Currency(rawValue: string) {
            return type
        }
        return .undefined
    }
}

// MARK: Public Properties

extension Currency {
    var country: String? {
        switch self {
        case .EUR, .AUD, .BGN, .BRL, .CAD, .CHF, .CNY, .CZK, .DKK, .GBP, .HKD, .HRK, .HUF, .IDR, .ILS, .INR, .ISK, .JPY, .KRW, .MXN, .MYR, .NOK, .NZD, .PHP, .PLN, .RON, .RUB, .SEK, .SGD, .THB, .TRY, .USD, .ZAR:
            return "localized_countryWithCurrency\(self.rawValue.uppercased())".localized
        case .undefined:
            return nil
        }
    }
    
    var countryImage: UIImage? {
        switch self {
        case .EUR, .AUD, .BGN, .BRL, .CAD, .CHF, .CNY, .CZK, .DKK, .GBP, .HKD, .HRK, .HUF, .IDR, .ILS, .INR, .ISK, .JPY, .KRW, .MXN, .MYR, .NOK, .NZD, .PHP, .PLN, .RON, .RUB, .SEK, .SGD, .THB, .TRY, .USD, .ZAR:
            let name = "gd-img-currency-\(self.rawValue.lowercased())"
            return UIImage(named: name)
        case .undefined:
            return nil
        }
    }
}
