//
//  CurrencyModel.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import Foundation

@objc protocol CurrencyListener: Listener {
    @objc optional func currency(didUpdateAmount amount: String?)
}

// MARK: Class Definition

class CurrencyModel: Listening {
    
    // MARK: Public Properties
    
    private(set) var listeners = ListenersContainer<CurrencyListener>()
    
    private(set) var currency = Currency.undefined
    private(set) var amount: String?
    
    // MARK: Init Methods & Superclass Overriders
    
    /// Initializes currency model with parameters.
    ///
    /// - Parameters:
    ///   - currency: Currency value.
    ///   - amount: Currency amount.
    required init(currency: Currency, amount: String?) {
        self.currency = currency
        self.amount = amount
    }
}

// MARK: Public Methods

extension CurrencyModel {
    /// Updates currency value.
    ///
    /// - Parameter currency: New currency.
    func updateWith(_ currency: Currency) {
        if self.currency == currency {
            return
        }
        
        self.amount = nil
        self.currency = currency
        self.listeners.invoke { (listener) in
            listener?.currency?(didUpdateAmount: self.amount)
        }
    }
    
    /// Updates currency amount value.
    ///
    /// - Parameter amount: New amount.
    func updateWith(_ amount: String?) {
        if self.amount == amount {
            return
        }
        
        self.amount = amount
        self.listeners.invoke { (listener) in
            listener?.currency?(didUpdateAmount: amount)
        }
    }
}
