//
//  NetworkRequestModel.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import Foundation

// MARK: Struct Definition

struct NetworkRequestModel {
    
    // MARK: Public Properties
    
    private(set) var url: URL!
    private(set) var parameters: [String : Any]?
    
    // MARK: Init Methods & Superclass Overriders
    
    /// Initializes request with parameters.
    ///
    /// - Parameters:
    ///   - address: Request address.
    ///   - parameters: Request parameters.
    ///   - dataVerifier: Data verifier.
    /// - Throws: Error with value, value key and model class.
    init(address: String, parameters: [String : Any]?, dataVerifier: DataVerifier) throws {
        self.url = try dataVerifier.url(existAndNotEmptyThrows: address)
        self.parameters = parameters
    }
}

// MARK: CustomStringConvertible

extension NetworkRequestModel: CustomStringConvertible {
    var description: String {
        var string = "address: \(self.url!)"
        if let parameters = self.parameters {
            string += "\nparameters: {"
            for (key, value) in parameters {
                string += "\n   \(key): \(self.objectDescription(value, level: 1))"
            }
            string += " };"
        } else {
            string += "\nparameters: [:]"
        }
        return string
    }
}

// MARK: Support Methods

private extension NetworkRequestModel {
    func objectDescription(_ object: Any, level: UInt) -> String {
        if let dictionary = object as? [String : Any] {
            var gap = ""
            for _ in 0..<level {
                gap += "   "
            }
            
            var string = " {"
            for (key, value) in dictionary {
                string += "\n   \(gap)\(key): \(self.objectDescription(value, level: (level + 1)))"
            }
            string += " \(gap)};"
            return string
        } else {
            let description = String(describing: object)
            if description.count > 50 {
                return description.prefix(50) + "..."
            } else {
                return description
            }
        }
    }
}
