//
//  RateModel.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import Foundation

@objc protocol RateListener: Listener {
    @objc optional func rate(didUpdateValue value: String)
}

// MARK: Class Definition

class RateModel: Listening {
    
    // MARK: Public Properties
    
    private(set) var listeners = ListenersContainer<RateListener>()
    
    private(set) var currency = Currency.undefined
    private(set) var value: String!
    
    // MARK: Init Methods & Superclass Overriders
    
    /// Initializes currency rate model with parameters.
    ///
    /// - Parameters:
    ///   - currency: Currency value.
    ///   - value: Rate value.
    ///   - dataVerifier: Data verifier.
    /// - Throws: An error if the value does not exist or is empty.
    required init(currency: Any?, value: Any?, dataVerifier: DataVerifier) throws {
        self.currency = Currency.valueToType(currency)
        self.value = try dataVerifier.string(existAndNotEmptyThrows: value)
    }
}

// MARK: Public Methods

extension RateModel {
    /// Updates currency rate value.
    ///
    /// - Parameter value: New value.
    func updateWith(_ value: String) {
        if self.value == value {
            return
        }
        
        self.value = value
        self.listeners.invoke { (listener) in
            listener?.rate?(didUpdateValue: value)
        }
    }
}
