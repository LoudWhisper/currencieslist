//
//  RatesContainerModel.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import Foundation

// MARK: Struct Definition

struct RatesContainerModel {
    
    // MARK: Struct
    
    private struct Keys {
        static let currency = "base"
        static let rates = "rates"
    }
    
    // MARK: Public Properties
    
    private(set) var currencyWithAmount: CurrencyModel!
    private(set) var currencies: [Currency]!
    private(set) var rateForCurrency: [String : RateModel]!
    
    // MARK: Init Methods & Superclass Overriders
    
    /// Initializes rates container model with parameters.
    ///
    /// - Parameters:
    ///   - response: Server response.
    ///   - dataVerifier: Data verifier.
    /// - Throws: An error if the base currency, date or rates does not exist or is empty.
    init(response: [String : Any], dataVerifier: DataVerifier) throws {
        self.currencyWithAmount = try self.currency(fromValue: response[Keys.currency])
        
        let rates = try self.rates(fromValue: response[Keys.rates], dataVerifier: dataVerifier)
        self.currencies = rates.currencies
        self.rateForCurrency = rates.rateForCurrency
    }
    
    /// Initializes empty rates container model with parameters.
    ///
    /// - Parameters:
    ///   - currency: Base currency.
    ///   - dataVerifier: Data verifier.
    /// - Throws: An error if the base currency is undefined.
    init(currency: Currency, dataVerifier: DataVerifier) throws {
        if currency == .undefined {
            // Somehow an undefined currency was chosen as the base.
            throw(DataVerifier.Errors.data(value: currency, result: nil))
        }
        self.currencyWithAmount = CurrencyModel(currency: currency, amount: nil)
        
        let rates = self.emptyRates(dataVerifier: dataVerifier)
        self.currencies = rates.currencies
        self.rateForCurrency = rates.rateForCurrency
    }
}

// MARK: Public Methods

extension RatesContainerModel {
    /// Updates currency rates values.
    ///
    /// - Parameter rates: Currency rates values.
    func updateWith(_ rates: [RateModel]) {
        for rate in rates {
            guard let containerRate = self.rateForCurrency[rate.currency.rawValue] else {
                continue
            }
            containerRate.updateWith(rate.value)
        }
    }
    
    /// Updates currency value.
    ///
    /// - Parameter currency: New currency.
    func updateWith(_ currency: Currency) {
        self.currencyWithAmount.updateWith(currency)
    }
    
    /// Updates currency amount value.
    ///
    /// - Parameter amount: New amount.
    func updateWith(_ amount: String?) {
        self.currencyWithAmount.updateWith(amount)
    }
}

// MARK: Parsing Methods

private extension RatesContainerModel {
    func currency(fromValue value: Any?) throws -> CurrencyModel {
        let currency = Currency.valueToType(value)
        if currency == .undefined {
            // Somehow an undefined currency was chosen as the base.
            throw(DataVerifier.Errors.data(value: value, result: currency))
        }
        return CurrencyModel(currency: currency, amount: nil)
    }
    
    func rates(fromValue value: Any?, dataVerifier: DataVerifier) throws -> (currencies: [Currency], rateForCurrency: [String : RateModel]) {
        guard let rates = value as? [String : Any], rates.count > 0 else {
            throw(DataVerifier.Errors.data(value: value, result: nil))
        }
        
        let emptyRates = self.emptyRates(dataVerifier: dataVerifier)
        let currencies = emptyRates.currencies
        var rateForCurrency = emptyRates.rateForCurrency
        for (currency, value) in rates {
            do {
                let rate = try RateModel(currency: currency, value: value, dataVerifier: dataVerifier)
                if rate.currency == .undefined {
                    // We can not display an undefined currency for users.
                    // Probably, the latest version is not installed and we do not want to report this.
                    continue
                }
                
                rateForCurrency[rate.currency.rawValue] = rate
            } catch let error {
                // TODO: Report an error in the analytics services. Remove print.
                print(error)
            }
        }
        return (currencies: currencies, rateForCurrency: rateForCurrency)
    }
    
    func emptyRates(dataVerifier: DataVerifier) -> (currencies: [Currency], rateForCurrency: [String : RateModel]) {
        let currencies = Currency.values()
        var rateForCurrency: [String : RateModel] = [:]
        for currency in currencies {
            do {
                let rate = try RateModel(currency: currency.rawValue, value: "-", dataVerifier: dataVerifier)
                rateForCurrency[rate.currency.rawValue] = rate
            } catch let error {
                // TODO: Report an error in the analytics services. Remove print.
                print(error)
            }
        }
        return (currencies: currencies, rateForCurrency: rateForCurrency)
    }
}
