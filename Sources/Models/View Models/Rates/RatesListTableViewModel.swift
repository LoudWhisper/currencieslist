//
//  RatesListTableViewModel.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import UIKit

protocol RatesListTableViewModelOutput: class {
    func rates(didSelect currency: Currency)
    func rates(didScroll contentOffset: CGPoint)
}

// MARK: Class Definition

class RatesListTableViewModel: NSObject {
    
    // MARK: Interface Builder Properties
    
    @IBOutlet private weak var tableView: UITableView?
    
    // MARK: Private Properties
    
    private weak var output: RatesListTableViewModelOutput?
    
    private var currencyWithAmount: CurrencyModel?
    private var rates: [RateModel]?
    private var selectedCurrencyIndex: Int?
    private var keyboardActive = false
    
    private lazy var numberFormatter: NumberFormatter = {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        return numberFormatter
    }()
}

// MARK: Public Methods

extension RatesListTableViewModel {
    /// Configures table view model with parameters.
    ///
    /// - Parameters:
    ///   - currencyWithAmount: Currency with amount.
    ///   - rates: Currency rates.
    ///   - output: Table view model output.
    func configure(withCurrency currencyWithAmount: CurrencyModel, rates: [RateModel], output: RatesListTableViewModelOutput) {
        self.output = output
        
        let reloadData = (self.rates == nil || self.rates?.count == 0)
        if !reloadData {
            return
        }
        
        self.currencyWithAmount = currencyWithAmount
        self.rates = rates
        self.selectedCurrencyIndex = self.index(forCurrency: self.currencyWithAmount?.currency)
        self.tableView?.reloadData()
    }
    
    /// Configures table view model with parameters.
    ///
    /// - Parameters:
    ///   - keyboardActive: True if keyboard active. False otherwise.
    func configure(withKeyboardActive keyboardActive: Bool) {
        self.keyboardActive = keyboardActive
        
        if let indexPath = self.indexPath(forCurrency: self.currencyWithAmount?.currency), let cell = self.tableView?.cellForRow(at: indexPath) as? CurrencyRateTableViewCell {
            self.configure(cell: cell, atIndexPath: indexPath)
        }
    }
}

// MARK: UITableViewDelegate & UITableViewDataSource

extension RatesListTableViewModel: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let _ = self.currencyWithAmount, let rates = self.rates else {
            return 0
        }
        return rates.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let rate = self.rate(forIndexPath: indexPath) {
            let previoslySelectedCurrency = self.currencyWithAmount?.currency
            self.selectedCurrencyIndex = self.index(forCurrency: rate.currency)
            self.output?.rates(didSelect: rate.currency)
            
            if indexPath.row != 0 {
                self.animateSelection(atIndexPath: indexPath, toIndexPath: self.indexPath(forCurrency: previoslySelectedCurrency))
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.currencyCellCellIdentifier(), for: indexPath) as! CurrencyRateTableViewCell
        self.configure(cell: cell, atIndexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
}

// MARK: UIScrollViewDelegate

extension RatesListTableViewModel: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.output?.rates(didScroll: scrollView.contentOffset)
    }
}

// MARK: Table View Methods

private extension RatesListTableViewModel {
    func configureVisibleCells() {
        guard let rates = self.rates, rates.count > 0 else {
            return
        }
        
        for index in 0..<rates.count {
            let indexPath = IndexPath(row: index, section: 0)
            guard let cell = self.tableView?.cellForRow(at: indexPath) as? CurrencyRateTableViewCell else {
                continue
            }
            self.configure(cell: cell, atIndexPath: indexPath)
        }
    }
    
    func configure(cell: CurrencyRateTableViewCell, atIndexPath indexPath: IndexPath) {
        guard let rate = self.rate(forIndexPath: indexPath), let currencyWithAmount = self.currencyWithAmount else {
            return
        }
        
        if indexPath.row == 0 {
            cell.configure(withCurrency: currencyWithAmount, showAmount: !self.keyboardActive)
        } else {
            cell.configure(withRate: rate, currencyWithAmount: currencyWithAmount, numberFormatter: self.numberFormatter)
        }
    }
    
    func animateSelection(atIndexPath indexPath: IndexPath, toIndexPath: IndexPath?) {
        self.tableView?.isUserInteractionEnabled = false
        
        CATransaction.begin()
        CATransaction.setCompletionBlock {
            self.tableView?.isUserInteractionEnabled = true
        }
        
        let selectedIndexPath = IndexPath(row: 0, section: 0)

        self.tableView?.beginUpdates()
        self.tableView?.moveRow(at: indexPath, to: selectedIndexPath)
        if let targetIndexPath = toIndexPath {
            self.tableView?.moveRow(at: selectedIndexPath, to: targetIndexPath)
        }
        self.tableView?.endUpdates()
        
        CATransaction.commit()
        
        self.configureVisibleCells()
    }
}

// MARK: Support Methods

private extension RatesListTableViewModel {
    func rate(forIndexPath indexPath: IndexPath) -> RateModel? {
        var index = indexPath.row
        if let selectedCurrencyIndex = self.selectedCurrencyIndex, index == 0 {
            index = selectedCurrencyIndex
        } else if let selectedCurrencyIndex = self.selectedCurrencyIndex {
            if index <= selectedCurrencyIndex {
                index -= 1
            }
        }
        if let rates = self.rates, rates.count > index, index >= 0 {
            return rates[index]
        }
        return nil
    }
    
    func index(forCurrency currency: Currency?) -> Int? {
        return self.rates?.index(where: { (rate) -> Bool in
            return (rate.currency == currency)
        })
    }
    
    func indexPath(forCurrency currency: Currency?) -> IndexPath? {
        let searchIndex = self.rates?.index(where: { (rate) -> Bool in
            return (rate.currency == currency)
        })
        
        if let searchCurrency = currency, let index = searchIndex {
            if self.currencyWithAmount != nil && self.currencyWithAmount?.currency == searchCurrency {
                return IndexPath(row: 0, section: 0)
            } else if let selectedCurrencyIndex = self.selectedCurrencyIndex {
                if index < selectedCurrencyIndex {
                    return IndexPath(row: (index + 1), section: 0)
                }
            }
            return IndexPath(row: index, section: 0)
        }
        return nil
    }
    
    func currencyCellCellIdentifier() -> String {
        return String(describing: CurrencyRateTableViewCell.self)
    }
}
