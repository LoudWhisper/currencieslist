//
//  RatesListViewModel.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import Foundation

protocol RatesListViewOutput: class {
    func applyState(_ state: RatesListViewController.State)
}

protocol RatesListViewInput: ControllerLifeCycleObservable {
    func currencySelected(_ currency: Currency)
    func amountChanged(_ amount: String?)
}

// MARK: Class Definition

class RatesListViewModel {
    
    // MARK: Enums
    
    enum State {
        case suspense(currencyWithAmount: CurrencyModel, rates: [RateModel])
    }
    
    // MARK: Private Properties
    
    private weak var router: RatesRoutesProviderable?
    private weak var output: RatesListViewOutput?
    
    private var dataModel: RatesListDataInput!
    
    // MARK: Init Methods & Superclass Overriders
    
    /// Initializes view model with parameters.
    ///
    /// - Parameters:
    ///   - router: Navigation router.
    ///   - output: View model output.
    required init(withRouter router: RatesRoutesProviderable, output: RatesListViewOutput) {
        self.router = router
        self.output = output
    }
}

// MARK: Public Methods

extension RatesListViewModel {
    /// Configures view model with data model.
    ///
    /// - Parameter dataModel: Data model.
    /// - Returns: Configured view model.
    func withDataModel(_ dataModel: RatesListDataInput) -> RatesListViewModel {
        self.dataModel = dataModel
        return self
    }
}

// MARK: ControllerLifeCycleObservable

extension RatesListViewModel: ControllerLifeCycleObservable {
    func controllersViewDidLoad() {
        self.dataModel.loadRates(forCurrency: nil)
    }
}

// MARK: RatesListViewInput

extension RatesListViewModel: RatesListViewInput {
    func currencySelected(_ currency: Currency) {
        self.dataModel.loadRates(forCurrency: currency)
    }
    
    func amountChanged(_ amount: String?) {
        self.dataModel.changeAmount(amount)
    }
}

// MARK: RatesListDataOutput

extension RatesListViewModel: RatesListDataOutput {
    func applyState(_ state: RatesListViewModel.State) {
        switch state {
        case .suspense(let currencyWithAmount, let rates):
            self.output?.applyState(.suspense(currencyWithAmount: currencyWithAmount, rates: rates))
        }
    }
}
