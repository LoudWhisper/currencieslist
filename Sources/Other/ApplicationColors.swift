//
//  ApplicationColors.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import UIKit

enum ApplicationColor {
    case cell
    case underline
    case view
    case text
}

extension ApplicationColor {
    var color: UIColor {
        switch self {
        case .cell:
            return UIColor.white
        case .underline:
            return UIColor(red: 32, green: 32, blue: 32)
        case .view:
            return UIColor.white
        case .text:
            return UIColor(red: 32, green: 32, blue: 32)
        }
    }
}
