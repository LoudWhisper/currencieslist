//
//  ListenersContainer.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import Foundation

@objc protocol Listener: class {
    
}

// MARK: Class Definition

class ListenersContainer<T> where T: Listener {
    
    // MARK: Private Properties
    
    private var listeners: Set<WeakObject<T>> = []
}

// MARK: Public Methods

extension ListenersContainer {
    /// Adds listener to the set of listeners.
    ///
    /// - Parameter weakListener: Listener in a weak object.
    func add(weakListener: WeakObject<T>) {
        self.listeners.insert(weakListener)
    }
    
    /// Removes listener from the set of listeners.
    ///
    /// - Parameter weakListener: Listener in a weak object.
    func remove(weakListener: WeakObject<T>) {
        self.listeners.remove(weakListener)
    }
    
    /// Passes through the weak objects of listeners.
    /// Removes freed listeners and invokes code for others.
    ///
    /// - Parameter invocation: Code to invoke.
    func invoke(invocation: (T?) -> ()) {
        for weakListener in self.listeners {
            if let listener = weakListener.value {
                invocation(listener)
            } else {
                self.remove(weakListener: weakListener)
            }
        }
    }
    
    /// Passes through the weak objects of listeners.
    /// Removes freed listeners before returning the count.
    ///
    /// - Returns: Listeners count
    func count() -> Int {
        for weakListener in self.listeners {
            if let _ = weakListener.value {
                continue
            }
            self.remove(weakListener: weakListener)
        }
        
        return self.listeners.count
    }
    
    /// Removes all listeners.
    func removeAll() {
        self.listeners.removeAll()
    }
}
