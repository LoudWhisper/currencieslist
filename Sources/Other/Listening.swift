//
//  Listening.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import Foundation

// MARK: Protocol Definition

protocol Listening {
    
    // MARK: Associatedtypes
    
    associatedtype T: Listener
    
    // MARK: Properties
    
    var listeners: ListenersContainer<T> { get }
    
    // MARK: Methods
    
    /// Adds listener.
    ///
    /// - Parameter listener: Listener to add.
    func add(listener: T)
    
    /// Removes listener.
    ///
    /// - Parameter listener: Listener to remove.
    func remove(listener: T)
}

// MARK: Default Implementation

extension Listening {
    func add(listener: T) {
        let weakListener = WeakObject(value: listener)
        self.listeners.add(weakListener: weakListener)
    }
    
    func remove(listener: T) {
        let weakListener = WeakObject(value: listener)
        self.listeners.remove(weakListener: weakListener)
    }
}
