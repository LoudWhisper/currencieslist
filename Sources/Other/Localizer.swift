//
//  Localizer.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import Foundation

// MARK: Class Definition

class Localizer {
    
    // Structs
    
    private struct Keys {
        static let value = "value"
    }
    
    private struct LocalizationFile {
        static let name = "Localizable"
        static let type = "plist"
    }
    
    // MARK: Public Properties
    
    static let shared = Localizer()
    
    // MARK: Public Properties
    
    private(set) var localizableValues: [String : [String : String]]?
    
    // MARK: Init Methods & Superclass Overriders
    
    init() {
        guard let path = Bundle.main.path(forResource: LocalizationFile.name, ofType: LocalizationFile.type), let localizableValues = NSDictionary(contentsOfFile: path) as? [String : [String : String]] else {
            return
        }
        self.localizableValues = localizableValues
    }
}

// MARK: Support Methods

fileprivate extension Localizer {
    /// Searches for a localized value for a string, using it as a key.
    ///
    /// - Parameter string: Localizable key.
    /// - Returns: Localized value or '**string**'.
    func localize(_ string: String) -> String {
        let localized = self.localizableValues?[string]
        guard let localizedString = localized?[Keys.value] else {
            return "**\(string)**"
        }
        return localizedString.replacingOccurrences(of: "\\n", with: "\n")
    }
}

extension String {
    var localized: String {
        return Localizer.shared.localize(self)
    }
}
