//
//  WeakObject.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import Foundation

// MARK: Class Definition

class WeakObject<T>: Hashable where T: AnyObject {
    
    // MARK: Hashable
    
    static func == (lhs: WeakObject<T>, rhs: WeakObject<T>) -> Bool {
        return (lhs.hashValue == rhs.hashValue)
    }
    
    var hashValue: Int {
        if let cachedHashValue = self.cachedHashValue {
            return cachedHashValue
        }
        
        guard let object = self.value else {
            return 0
        }
        
        let address = Unmanaged.passUnretained(object).toOpaque()
        let hash = String(describing: object).hashValue ^ address.hashValue
        return hash
    }
    
    // MARK: Private Properties
    
    private var cachedHashValue: Int?
    
    // MARK: Public Properties
    
    private(set) weak var value: T?
    
    // MARK: Init Methods & Superclass Overriders
    
    required init(value: T?) {
        self.value = value
        self.cachedHashValue = hashValue
    }
}
