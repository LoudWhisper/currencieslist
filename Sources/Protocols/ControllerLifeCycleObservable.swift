//
//  ControllerLifeCycleObservable.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import Foundation

protocol ControllerLifeCycleObservable: class {
    /// Called on viewDidLoad.
    func controllersViewDidLoad()
    
    /// Called on viewWillAppear.
    func controllersViewWillAppear()
    
    /// Called on viewDidAppear.
    func controllersViewDidAppear()
    
    /// Called on viewWillDisappear.
    func controllersViewWillDisappear()
    
    /// Called on viewDidDisappear.
    func controllersViewDidDisappear()
    
    /// Called on viewDidLayoutSubviews.
    func controllersViewDidLayoutSubviews()
}

extension ControllerLifeCycleObservable {
    func controllersViewDidLoad() {
        // nothing to do
    }
    
    func controllersViewWillAppear() {
        // nothing to do
    }
    
    func controllersViewDidAppear() {
        // nothing to do
    }
    
    func controllersViewWillDisappear() {
        // nothing to do
    }
    
    func controllersViewDidDisappear() {
        // nothing to do
    }
    
    func controllersViewDidLayoutSubviews() {
        // nothing to do
    }
}
