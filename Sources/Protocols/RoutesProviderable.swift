//
//  RoutesProviderable.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import UIKit

// MARK: InitialRoutesProviderable

protocol InitialRoutesProviderable: class {
    /// Shows initial screen in window.
    ///
    /// - Parameter window: Application key window.
    func showInitialScreen(withWindow window: UIWindow)
}

// MARK: RootRoutesProviderable

protocol RootRoutesProviderable: class {
    /// Shows root screen in navigation controller.
    func showRootScreen()
}

// MARK: AuthorizedRoutesProviderable

protocol RatesRoutesProviderable: class {
    /// Shows rates screen.
    func showRatesScreen()
}
