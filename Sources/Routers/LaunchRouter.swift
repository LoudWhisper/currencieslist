//
//  LaunchRouter.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import UIKit

// MARK: Class Definition

class LaunchRouter: Router {
    
    // MARK: Enums
    
    enum Errors: Error {
        case window
    }
    
    // MARK: Private Properties
    
    private var window: UIWindow?
    
    // MARK: Init Methods & Superclass Overriders
    
    override init() {
        super.init()
    }
}

// MARK: InitialRoutesProviderable

extension LaunchRouter: InitialRoutesProviderable {
    func showInitialScreen(withWindow window: UIWindow) {
        self.window = window
        self.showRatesScreen()
        window.makeKeyAndVisible()
    }
}

// MARK: RatesRoutesProviderable

extension LaunchRouter: RatesRoutesProviderable {
    func showRatesScreen() {
        let window = self.applicationWindow()
        let router = RatesRouter(withParentRouter: self, servicesAssembly: self.servicesAssembly, screensAssembly: self.screensAssembly, navigationController: self.navigationController)
        router.showInitialScreen(withWindow: window)
        self.childRouter = router
    }
}

// MARK: Support Methods

private extension LaunchRouter {
    func currentExistedWindow() throws -> UIWindow  {
        guard let window = self.window else {
            throw(Errors.window)
        }
        return window
    }
    
    func applicationWindow() -> UIWindow {
        var window: UIWindow!
        do {
            window = try self.currentExistedWindow()
        } catch let error {
            // TODO: Report an error in the analytics services. Remove print.
            print(error)
        }
        return window
    }
}
