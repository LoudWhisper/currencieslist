//
//  RatesRouter.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import UIKit

// MARK: Class Definition

class RatesRouter: Router {
    
    // MARK: Init Methods & Superclass Overriders
    
    override init(withParentRouter parentRouter: Router, servicesAssembly: ServicesAssembly, screensAssembly: ScreensAssembly, navigationController: UINavigationController) {
        super.init(withParentRouter: parentRouter, servicesAssembly: servicesAssembly, screensAssembly: screensAssembly, navigationController: navigationController)
    }
}

// MARK: InitialRoutesProviderable

extension RatesRouter: InitialRoutesProviderable {
    func showInitialScreen(withWindow window: UIWindow) {
        let viewController = self.screensAssembly.viewController(withType: .rates(screenContainer: .list(router: self)))
        self.navigationController.setViewControllers([viewController], animated: false)
        
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = kCATransitionFade
        transition.subtype = nil
        
        window.layer.add(transition, forKey: kCATransitionFade)
        window.rootViewController = self.navigationController
    }
}

// MARK: RatesRoutesProviderable

extension RatesRouter: RatesRoutesProviderable {
    func showRatesScreen() {
        self.navigationController.popToRootViewController(animated: true)
    }
}
