//
//  Router.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import UIKit

// MARK: Class Definition

class Router {
    
    // MARK: Public Properties
    
    var childRouter: Router?
    
    private(set) weak var parentRouter: Router?
    private(set) var servicesAssembly: ServicesAssembly!
    private(set) var screensAssembly: ScreensAssembly!
    private(set) var navigationController: UINavigationController!
    
    // MARK: Init Methods & Superclass Overriders
    
    /// Initializes router and creates services assembly and screens assembly.
    init() {
        self.servicesAssembly = ServicesAssembly()
        self.screensAssembly = ScreensAssembly(withServicesAssembly: self.servicesAssembly)
        self.navigationController = self.screensAssembly.navigationController(withType: .common(screenContainer: .navigation))
    }
    
    /// Initializes router with parameters.
    ///
    /// - Parameters:
    ///   - parentRouter: The router that holds this router.
    ///   - servicesAssembly: Services assembly
    ///   - screensAssembly: Screens assembly
    ///   - navigationController: Root navigation controller for this router.
    init(withParentRouter parentRouter: Router, servicesAssembly: ServicesAssembly, screensAssembly: ScreensAssembly, navigationController: UINavigationController) {
        self.parentRouter = parentRouter
        self.servicesAssembly = servicesAssembly
        self.screensAssembly = screensAssembly
        self.navigationController = navigationController
    }
}
