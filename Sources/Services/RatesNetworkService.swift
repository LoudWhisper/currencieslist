//
//  RatesNetworkService.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import Foundation

// MARK: Class Definition

class RatesNetworkService {
    
    // MARK: Structs
    
    private struct Keys {
        static let baseCurrency = "base"
    }
    
    // MARK: Private Properties
    
    private var networkRequestsHandler: NetworkRequestsHandler!
    private var targetValuesProvider: TargetValuesProvider!
    
    // MARK: Init Methods & Superclass Overriders
    
    /// Initializes authorization network service with parameters.
    ///
    /// - Parameters:
    ///   - networkRequestsHandler: Network requests handler.
    ///   - targetValuesProvider: Target values provider.
    required init(networkRequestsHandler: NetworkRequestsHandler, targetValuesProvider: TargetValuesProvider) {
        self.networkRequestsHandler = networkRequestsHandler
        self.targetValuesProvider = targetValuesProvider
    }
}

// MARK: Public Methods

extension RatesNetworkService {
    /// Loads list of rates for currency.
    ///
    /// - Parameters:
    ///   - currency: Currency.
    ///   - onSuccess: Block. Called when the request succeeds.
    ///   - onFailure: Block. Called when the request fails.
    ///   - onCancellation: Block. Called when the request cancels.
    ///   - onSessionTask: Block. Called when session task created.
    func loadRatesListWith(currency: Currency, onSuccess: NetworkRequestsHandler.SuccessBlock?, onFailure: NetworkRequestsHandler.FailureBlock?, onCancellation: NetworkRequestsHandler.CancellationBlock?, onSessionTask: NetworkRequestsHandler.SessionTaskBlock?) {
        let parameters: [String : Any] = [Keys.baseCurrency : currency.rawValue.uppercased()]
        self.networkRequestsHandler.get(.rates(pathContainer: .list), parameters: parameters, onSuccess: onSuccess, onFailure: onFailure, onCancellation: onCancellation, onSessionTask: onSessionTask)
    }
}
