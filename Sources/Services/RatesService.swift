//
//  RatesService.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import Foundation

// MARK: Class Definition

class RatesService {
    
    // MARK: Private Properties
    
    private var dataVerifier: DataVerifier!
    
    private var ratesContainer: RatesContainerModel?
    
    // MARK: Init Methods & Superlcass Overriders
    
    /// Initializes announcements service with parameters.
    ///
    /// - Parameters:
    ///   - dataVerifier: Data verifier.
    required init(dataVerifier: DataVerifier) {
        self.dataVerifier = dataVerifier
    }
}

// MARK: Public Methods

extension RatesService {
    /// Processes server response after successful rates list loading.
    ///
    /// - Parameters:
    ///   - response: Server response.
    func ratesListDidLoad(withResponse response: [String : Any]) {
        do {
            let container = try RatesContainerModel(response: response, dataVerifier: self.dataVerifier)
            self.updateRatesContainer(withContainer: container)
        } catch let error {
            // TODO: Report an error in the analytics services. Remove print.
            print(error)
        }
    }
    
    /// Searches for rates container for currency. Creates empty one if not found.
    ///
    /// - Parameter currency: Selected currency.
    /// - Returns: Currency with amount and rates.
    func ratesForCurrency(_ currency: Currency?) -> (currencyWithAmount: CurrencyModel, rates: [RateModel]) {
        let container = self.ratesContainer(forCurrency: currency)
        let rates = self.rates(fromContainer: container)
        return (currencyWithAmount: container.currencyWithAmount, rates: rates)
    }
    
    /// Updates currency value.
    ///
    /// - Parameter currency: New currency.
    func updateCurrency(_ currency: Currency) {
        self.ratesContainer?.updateWith(currency)
    }
    
    /// Updates currency amount value.
    ///
    /// - Parameter amount: New amount.
    func updateAmount(_ amount: String?) {
        self.ratesContainer?.updateWith(amount)
    }
}

// MARK: Support Methods

private extension RatesService {
    func ratesContainer(forCurrency currency: Currency?) -> RatesContainerModel {
        if let selectedCurrency = currency {
            guard let container = self.ratesContainer else {
                let container = self.createRatesContainer(withCurrency: selectedCurrency)
                self.ratesContainer = container
                return container
            }
            
            if container.currencyWithAmount.currency != selectedCurrency {
                container.updateWith(selectedCurrency)
            }
            return container
        } else {
            guard let container = self.ratesContainer else {
                let currency = self.localCurrency()
                let container = self.createRatesContainer(withCurrency: currency)
                self.ratesContainer = container
                return container
            }
            return container
        }
    }
    
    func createRatesContainer(withCurrency currency: Currency) -> RatesContainerModel {
        do {
            let container = try RatesContainerModel(currency: currency, dataVerifier: self.dataVerifier)
            return container
        } catch let error {
            fatalError("Rates container with \(currency.rawValue) not found - \(error).")
        }
    }
    
    func localCurrency() -> Currency {
        let numberFormatter = NumberFormatter()
        let currencyCode = numberFormatter.currencyCode
        let currency = Currency.valueToType(currencyCode)
        if currency == .undefined {
            // TODO: find the most likely currency
            return .USD
        }
        return currency
    }
    
    func rates(fromContainer container: RatesContainerModel) -> [RateModel] {
        var rates: [RateModel] = []
        for currency in container.currencies {
            guard let rate = container.rateForCurrency[currency.rawValue] else {
                continue
            }
            rates.append(rate)
        }
        return rates
    }
    
    func updateRatesContainer(withContainer container: RatesContainerModel) {
        if let currenciesContainer = self.ratesContainer {
            if currenciesContainer.currencyWithAmount.currency != container.currencyWithAmount.currency {
                self.ratesContainer?.updateWith(container.currencyWithAmount.currency)
            }
            
            let rates = Array(container.rateForCurrency.values)
            self.ratesContainer?.updateWith(rates)
        } else {
            self.ratesContainer = container
        }
    }
}
