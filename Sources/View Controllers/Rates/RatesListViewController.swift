//
//  RatesListViewController.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import UIKit

// MARK: Class Definition

class RatesListViewController: UIViewController {
    
    // MARK: Enums
    
    enum State {
        case suspense(currencyWithAmount: CurrencyModel, rates: [RateModel])
    }
    
    // MARK: Interface Builder Properties
    
    @IBOutlet private weak var amountTextField: UITextField?
    @IBOutlet private weak var tableViewModel: RatesListTableViewModel?
    
    // MARK: Properties Overriders
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: Public Properties
    
    var viewModel: RatesListViewInput!
    
    // MARK: Private Properties
    
    private var currencyWithAmount: CurrencyModel?
    
    // MARK: Init Methods & Superclass Overriders
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "localized_conversionsTitle".localized
        self.view.backgroundColor = ApplicationColor.view.color

        self.configureStyle()
        self.viewModel.controllersViewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: RatesListViewOutput

extension RatesListViewController: RatesListViewOutput {
    func applyState(_ state: RatesListViewController.State) {
        switch state {
        case .suspense(let currencyWithAmount, let rates):
            self.currencyWithAmount = currencyWithAmount
            self.tableViewModel?.configure(withCurrency: currencyWithAmount, rates: rates, output: self)
        }
    }
}

// MARK: RatesListTableViewModelOutput

extension RatesListViewController: RatesListTableViewModelOutput {
    func rates(didSelect currency: Currency) {
        self.viewModel.currencySelected(currency)
        self.showAmountTextField()
    }
    
    func rates(didScroll contentOffset: CGPoint) {
        self.hideAmountTextField()
    }
}

// MARK: Configurations

private extension RatesListViewController {
    func configureStyle() {
        self.amountTextField?.font = UIFont.systemFont(ofSize: 30.0, weight: .medium)
        self.amountTextField?.textColor = ApplicationColor.text.color
    }
}

// MARK: Support Methods

private extension RatesListViewController {
    func showAmountTextField() {
        self.amountTextField?.text = self.currencyWithAmount?.amount
        
        guard let amountTextField = self.amountTextField, amountTextField.isHidden || !amountTextField.isFirstResponder else {
            return
        }
        
        self.amountTextField?.isHidden = false
        self.amountTextField?.becomeFirstResponder()
        self.tableViewModel?.configure(withKeyboardActive: true)
    }
    
    func hideAmountTextField() {
        guard let amountTextField = self.amountTextField, !amountTextField.isHidden || amountTextField.isFirstResponder else {
            return
        }
        
        self.amountTextField?.isHidden = true
        self.amountTextField?.resignFirstResponder()
        self.tableViewModel?.configure(withKeyboardActive: false)
    }
}

// MARK: Controls Actions

private extension RatesListViewController {
    @IBAction func amountTextFieldChanged(_ sender: UITextField) {
        self.viewModel.amountChanged(sender.text)
    }
}
