//
//  CurrencyRateTableViewCell.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import UIKit

// MARK: Class Definition

class CurrencyRateTableViewCell: UITableViewCell {
    
    // MARK: Interface Builder Properties
    
    @IBOutlet private weak var contentContainer: UIView?
    @IBOutlet private weak var currencyImageView: UIImageView?
    @IBOutlet private weak var currencyCodeLabel: UILabel?
    @IBOutlet private weak var currencyCountryLabel: UILabel?
    @IBOutlet private weak var currencyValueLabel: UILabel?
    @IBOutlet private weak var underlineView: TextUnderlineView?
    
    // MARK: Private Properties
    
    private var rate: RateModel?
    private var currencyWithAmount: CurrencyModel?
    private var numberFormatter: NumberFormatter?
    
    // MARK: Init Methods & Superclass Overriders

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.configureStyle()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .beginFromCurrentState, animations: {
            self.contentContainer?.alpha = (highlighted ? 0.25 : 1.0)
        }, completion: nil)
    }
}

// MARK: Public Methods

extension CurrencyRateTableViewCell {
    /// Configures cell with parameters.
    ///
    /// - Parameters:
    ///   - rate: Currency rate.
    ///   - currencyWithAmount: Currency with amount.
    ///   - numberFormatter: Number formatter.
    func configure(withRate rate: RateModel, currencyWithAmount: CurrencyModel, numberFormatter: NumberFormatter) {
        self.rate?.remove(listener: self)
        self.rate = rate
        self.rate?.add(listener: self)
        
        self.currencyWithAmount?.remove(listener: self)
        self.currencyWithAmount = currencyWithAmount
        self.currencyWithAmount?.add(listener: self)
        
        self.numberFormatter = numberFormatter
        
        self.currencyImageView?.image = rate.currency.countryImage
        self.currencyCodeLabel?.text = rate.currency.rawValue.uppercased()
        self.currencyCountryLabel?.text = rate.currency.country
        self.currencyValueLabel?.textColor = ApplicationColor.text.color.withAlphaComponent(0.5)
        self.currencyValueLabel?.text = self.value(withAmount: currencyWithAmount.amount, rate: rate.value, numberFormatter: numberFormatter)
        self.currencyValueLabel?.isHidden = false
        self.underlineView?.configure(withState: .inactive)
        self.contentContainer?.alpha = 1.0
        
    }
    
    /// Configures cell with parameters.
    ///
    /// - Parameters:
    ///   - currencyWithAmount: Currency with amount.
    ///   - showAmount: True to show amount. False otherwise.
    func configure(withCurrency currencyWithAmount: CurrencyModel, showAmount: Bool) {
        self.rate?.remove(listener: self)
        self.rate = nil
        
        self.currencyWithAmount?.remove(listener: self)
        self.currencyWithAmount = currencyWithAmount
        self.currencyWithAmount?.add(listener: self)
        
        self.currencyImageView?.image = currencyWithAmount.currency.countryImage
        self.currencyCodeLabel?.text = currencyWithAmount.currency.rawValue.uppercased()
        self.currencyCountryLabel?.text = currencyWithAmount.currency.country
        self.currencyValueLabel?.textColor = ApplicationColor.text.color
        self.currencyValueLabel?.text = (currencyWithAmount.amount ?? "-")
        self.currencyValueLabel?.isHidden = !showAmount
        self.underlineView?.configure(withState: .active)
        self.contentContainer?.alpha = 1.0
    }
}

// MARK: RateListener

extension CurrencyRateTableViewCell: RateListener {
    func rate(didUpdateValue value: String) {
        guard let _ = self.rate, let numberFormatter = self.numberFormatter else {
            return
        }
        self.currencyValueLabel?.text = self.value(withAmount: self.currencyWithAmount?.amount, rate: value, numberFormatter: numberFormatter)
    }
}

// MARK: CurrencyListener

extension CurrencyRateTableViewCell: CurrencyListener {
    func currency(didUpdateAmount amount: String?) {
        if let rate = self.rate, let numberFormatter = self.numberFormatter {
            self.currencyValueLabel?.text = self.value(withAmount: amount, rate: rate.value, numberFormatter: numberFormatter)
        } else {
            self.currencyValueLabel?.text = (amount ?? "-")
        }
    }
}

// MARK: Configurations

private extension CurrencyRateTableViewCell {
    func configureStyle() {
        self.backgroundColor = ApplicationColor.cell.color
        self.contentView.backgroundColor = ApplicationColor.cell.color
        
        self.currencyImageView?.layer.masksToBounds = true
        self.currencyImageView?.contentMode = .scaleAspectFill
        self.currencyImageView?.layer.cornerRadius = 25.0
        self.currencyImageView?.layer.borderWidth = 2.0
        self.currencyImageView?.layer.borderColor = ApplicationColor.text.color.cgColor
        
        self.currencyCodeLabel?.font = UIFont.systemFont(ofSize: 18.0, weight: .heavy)
        self.currencyCodeLabel?.textColor = ApplicationColor.text.color
        
        self.currencyCountryLabel?.font = UIFont.systemFont(ofSize: 14.0, weight: .regular)
        self.currencyCountryLabel?.textColor = ApplicationColor.text.color.withAlphaComponent(0.5)
        
        self.currencyValueLabel?.font = UIFont.systemFont(ofSize: 30.0, weight: .medium)
        self.currencyValueLabel?.textColor = ApplicationColor.text.color
    }
}

// MARK: Support Methods

private extension CurrencyRateTableViewCell {
    func value(withAmount amount: String?, rate: String, numberFormatter: NumberFormatter) -> String? {
        let rateNumber = NSDecimalNumber(string: rate)
        if let amountValue = amount, Double(amountValue) != nil {
            let amountNumber = NSDecimalNumber(string: amount)
            let value = amountNumber.multiplying(by: rateNumber)
            return numberFormatter.string(from: value)
        }
        return numberFormatter.string(from: rateNumber)
    }
}
