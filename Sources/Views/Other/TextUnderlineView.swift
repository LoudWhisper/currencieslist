//
//  TextUnderlineView.swift
//  CurrencyList
//
//  Created by Daniil Gavrilov on 08.07.2018.
//  Copyright © 2018 Daniil Gavrilov. All rights reserved.
//

import UIKit

// MARK: Class Definition

class TextUnderlineView: UIView {
    
    // MARK: Enums
    
    enum State {
        case active
        case inactive
    }
    
    // MARK: Init Methods & Superclass Overriders
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

// MARK: Public Methods

extension TextUnderlineView {
    /// Configures view with paremeters.
    ///
    /// - Parameter state: View state.
    func configure(withState state: State) {
        switch state {
        case .active:
            self.backgroundColor = ApplicationColor.underline.color
        case .inactive:
            self.backgroundColor = ApplicationColor.underline.color.withAlphaComponent(0.5)
        }
    }
}
